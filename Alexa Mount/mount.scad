$fn = 160;

width = 50;

difference()
{
    cylinder(h=37, d=90);
    
    translate([0,0,3])
        alexa();
    
    translate([width/2, -45, -0.1])
        cube([90, 90, 40.2]);
    translate([-(width/2)-90, -45, -0.1])
        cube([90, 90, 40.2]);
}



module alexa()
{
    cylinder(h=32, d=84);
    cylinder(h=40, d=79);
}