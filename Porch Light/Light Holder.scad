include <threads.scad>

$fn = 100;

difference()
{
	cylinder(h = 60, d = 90);

	translate([0,0,20])
		cylinder(h = 40.1, d1 = 18, d2 = 85);
	translate([0,0,10])
		cylinder(h = 10.1, d = 18);
	translate([0,0,-0.1])
	{
		cylinder(h = 10.2, d = 6);

		// Bulb holder screw holes
		translate([6,0,0])
			cylinder(h = 10.2, d=3.2, $fn=30);
		translate([-6,0,0])
			cylinder(h = 10.2, d=3.2, $fn=30);

		// Main screw holes
		translate([34,0,0])
		{
			cylinder(h = 60.2, d=4.4, $fn=30);
			translate([0,0,2.4]) cylinder(h = 60.2, d=8.2, $fn=30);
		}
		translate([-34,0,0])
		{
			cylinder(h = 60.2, d=4.4, $fn=30);
			translate([0,0,2.4]) cylinder(h = 60.2, d=8.2, $fn=30);	
		}


		translate([-10,-5,0])
			cube([20,50,8]);

		translate([-10,22,0])
			cube([20,28,14]);
	}
}

translate([0,0,60])
{
	difference()
	{
		cylinder(h=3, d=90);
		metric_thread(85, 1.5, 3, internal=true, leadin=1);
	}
}