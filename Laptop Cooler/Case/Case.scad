include <modules.scad>

length = 38.5;
width = 50;
height = 24;

wall = 2;

outer_length = length + wall + wall;
outer_width = width + wall + wall;
outer_height = height + wall;


difference()
{
    cube([outer_length, outer_width, outer_height]);
    translate([wall, wall, wall])
    {
        cube([length, width, height + 0.1]);
    }
    
    // Power jack
    rotate([0,90,0])
    {
        translate([-((height / 2) + wall), 35, outer_length - wall - 0.1])
            cylinder(h = wall + 0.2, d = 12.2, $fn=50);
    }    

    // Switch
    rotate([0,90,0])
    {
        translate([-((height / 2) + wall), 35, -0.1])
            cylinder(h = wall + 0.2, d = 15.4, $fn=50);
    }
    
    translate([wall + 6, -0.1, wall])
    {
        cube([length - 12, wall + 0.2,  10]);
    }
}

translate([-10, 10, 0]) post();
translate([outer_length, 10, 0]) post();

translate([outer_length / 2, 15, wall])
{
    post = 3;
    
    quads(5.5, length - (post / 2))
    {
        translate([0,0,height / 2])
        {
            cube([post, post, height], center = true);
        }
    }
}

translate([0,0,(outer_height/2)])
{
    translate([0,20,0]) rotate([0,0,90]) fillet(5, outer_height, 30);
    translate([0,10,0]) rotate([0,0,180]) fillet(5, outer_height, 30);
    
    translate([outer_length,20,0]) rotate([0,0,0]) fillet(5, outer_height, 30);
    translate([outer_length,10,0]) rotate([0,0,-90]) fillet(5, outer_height, 30);
}

module post()
{
    difference()
    {
        cube([10,10,height + wall]);
        translate([5,5,0])
        {
            cylinder(h = height + wall + 0.1, d = 5.8, $fn=30);
        }
    }
}