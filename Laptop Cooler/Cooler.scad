include <modules.scad>
include <threads.scad>

thickness = 6;
fn = 30;


module all()
{

	mirror_copy([0,1,0])
	{
		translate([0,0.5,0])
		{
			base();
			translate([70,90,-25])
				fan();
		}
	}


	translate([0, -27.5, -5])
		middle();
}


foot2();

module latch()
{
	difference()
	{
		cube([35, 55, 2]);
		translate([10, 27.5, -0.1])
		{
			translate([0, 15, 0])
				cylinder(h = 5.2, d = 10.5, $fn=80);
			translate([0, -15, 0])
				cylinder(h = 5.2, d = 10.5, $fn=80);
		}
        
        translate([10, 27.5, 0])
        {
            cylinder(h = 5, d1 = 15, d2=15, $fn=60);
        }
	}
    
    translate([33,0,0])
        cube([2, 55, 7]);
}


module screw()
{
	
cylinder(h = 3, d = 15, $fn=6);
	metric_thread(10, 2, 20, internal=false, leadin=1);
}

module nut()
{
	difference()
	{
		cylinder(h = 4.5, d = 15, $fn=6);
		metric_thread(11, 2, 4.5, internal=true);
	}
}

module template()
{
    difference()
	{
		cube([120, 120, 0.8]);

		translate([60, 60, -0.1])
		{
			cylinder(h = 4.2, d = 114, $fn=80);
			quads(105, 105)
			{
				cylinder(h = thickness + 0.2, d = 4, $fn=30);
			}
		}
	}
}

module wrench()
{
    difference()
    {
        union()
        {
            cylinder(h = 8, d = 20, $fn=50);
            translate([20,0,5])
            {
                stadium(20, 3, 40);
            }
        }
        cylinder(h = 5, d = 15.4, $fn=6);
        translate([40, 0, 5])
        {
            cylinder(h = 5, d = 6, $fn=30);
        }
    }
    

}


module base()
{
	difference()
	{
		cube([230, 180, 4]);

		translate([70, 90, -0.1])
		{
			cylinder(h = 4.2, d = 114, $fn=80);
			quads(105, 105)
			{
				cylinder(h = thickness + 0.2, d = 4, $fn=30);
			}
		}
	}
}

module middle()
{
	difference()
	{
		cube([20, 55, 5]);
		translate([10, 27.5, -0.1])
		{
			translate([0, 15, 0])
                metric_thread(10.8, 2, 5.2, internal=true);
				//cylinder(h = 5.2, d = 10.5, $fn=50);
			translate([0, -15, 0])
                metric_thread(10.8, 2, 5.2, internal=true);
				//cylinder(h = 5.2, d = 10.5, $fn=50);
		}
	}

	translate([10, 27.5, -25])
	{
		leg(25, 10);
        translate([0,0,20])
            cylinder(h = 5, d1 = 10, d2=14, $fn=30);
	}
}

module leg_threaded()
{
    difference()
    {
        translate([0,0,(15 / 4)])
        {
            cylinder(h = 30 - (15 / 4), d = 15, $fn=30);
            scale([1,1,0.5])
                sphere(d = 15, $fn=30);
        }
        translate([0,0,10])
        {
            metric_thread(10.8, 2, 20.1, internal=true);
        }
    }
}

module leg(height, diameter)
{
	translate([0,0,(diameter / 4)])
	{
		cylinder(h = height - (diameter / 4), d = diameter, $fn=30);
		scale([1,1,0.5])
			sphere(d = diameter, $fn=30);
	}
}

module foot()
{
	w = 1.5;
	difference()
	{
		leg(12, 10 + (w * 2));
		translate([0,0,w]) leg(12, 10);
	}
}

module foot2()
{
	difference()
	{
		union()
		{
			cylinder(h = 12 - (15 / 4), d = 19, $fn=30);
			scale([1,1,0.5])
				sphere(d = 19, $fn=30);
		}

		translate([0,0,2])
		{
			cylinder(h = 12 - (15 / 4), d = 15, $fn=30);
			scale([1,1,0.5])
				sphere(d = 15, $fn=30);
		}
	}
}

module fan()
{
	color([1,0,0])
		rounded_rect(120, 120, 25, 5);
}

module laptop()
{
	difference()
	{
		color([0,0,1])
			translate([-127.5, -180, -15])
				cube([255, 360, 15]);

		translate([110, -180.5, 0])
			rotate([0,31.8,0])
				cube([20.5, 361, 10]);

		translate([127.5, -180.5, -10.5])
			rotate([0,-31.8,90])
				cube([20.5, 256, 10]);
		
		translate([127.5, 185, 0])
			rotate([0,31.8 + 180,90])
				cube([20.5, 256, 10]);
	}

	rotate([0,0,90])
		translate([-157,-108,0])
			base_front();
}