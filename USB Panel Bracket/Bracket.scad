n = 15;
h = 10;

linear_extrude(h)
{
	polygon([[0,0], [n,0], [0,n], [0,0]]);
}

translate([n/2, 0, h/2])
{
	rotate([90,0,0])
	{
		cylinder(h = 5, d = 4, $fn=30);
	}
}

translate([-5, n/2, h/2])
{
	rotate([0,90,0])
	{
		cylinder(h = 5, d = 4, $fn=30);
	}
}