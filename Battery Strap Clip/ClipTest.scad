bl = 1;
bw = 77;
bh = 20;

sw = 52;

thickness = 2;

difference()
{
	union()
	{
		clip(75);

		translate([offset_x, 22, thickness])
		{
			for(x = [0 : 1 : 10])
			{
				for(y = [0 : 1 : 6])
				{
					translate([x * 5, y * 5, 0])
						sphere(d=0.8,$fn=10);
						//cylinder(h = 0.4, d1=1, d2=0.5, $fn=10);
				}
			}
		}
	}

	offset_x = ((bw + (thickness * 2)) / 2) - (sw / 2);

	translate([offset_x, 12, -0.1])
		cube([sw, 8, thickness + 0.2]);

	translate([offset_x, 75 - 12 - 8, -0.1])
		cube([sw, 8, thickness + 0.2]);
}

module clip(width)
{
	translate([thickness,0,0])
		cube([bw, width, thickness]);

	translate([thickness,width,thickness])
		rotate([90,0,0])
			cylinder(h=width, r=thickness, $fn=30);

	translate([thickness+bw,width,thickness])
		rotate([90,0,0])
			cylinder(h=width, r=thickness, $fn=30);



	translate([0,0,thickness])
	{
		cube([thickness, width, bh]);
		translate([thickness + bw,0,0]) cube([thickness, width, bh]);
	}

	rotate([270,270,0])
		translate([thickness,thickness,0])
			wedge(width);

	translate([thickness+bw,width,thickness])
		rotate([90,270,0])
			wedge(width);

	translate([thickness,width,bh+thickness])
		rotate([90,90,0])
			cut_wedge(width);

	translate([thickness+bw,0,bh+thickness])
		rotate([90,90,180])
			cut_wedge(width);
}

module wedge(length)
{
	linear_extrude(length)
	{
		polygon([
			[0,0],
			[2.5,0],
			[0,6],
			[0,0]
		]);
	}
}

module cut_wedge(length)
{
	linear_extrude(length)
	{
		polygon([
			[0,0],
			[2.5,0],
			[1.25,3],
			[0,3],
			[0,0]
		]);
	}
}