include <modules.scad>

$fn = 60;

difference()
{
    union()
    {
        rounded_rect(40, 40, 2, 4);
        translate([0,0,2])
        {
            cylinder(h=4, d1=40, d2=34);
            translate([0,0,4])
            {
                cylinder(h=20, d1=34, d2=32);
                translate([0,0,10])
                    tarus(33/2, 0.5);
            }
        }   
    }

    translate([0,0,-0.1])
    {
        cylinder(h = 52.2, d = 30);
        cylinder(h=5, d1=38, d2=30);
    }
    quads(32, 32)
    {
        translate([0,0,-0.1]) cylinder(h = 2.2, d = 4);
    }
}

module tarus(r1, r2)
{
    rotate_extrude(convexity = 10)
        translate([r1, 0, 0])
            circle(r = r2, $fn = 100);
}