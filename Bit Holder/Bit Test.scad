difference() {
    cube([88,10,15]);
    for(i = [0 : 1 : 11]) {
        translate([(7 * i) + 5, 5, 3]) {
            hexagon (3.5 + (i * 0.1), 30);
        }
    }
}    


module hexagon(w, h) {
    translate([0,0,h/2]) {
        hull() {
            cube([w / 1.7, w, h], center = true);
            rotate([0,0,120]) {
                cube([w / 1.7, w, h], center = true);
            }
        }
    }
}