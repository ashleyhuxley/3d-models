hinge_width = 6;
walls = 3;
hex = 4.4;
box_corner_radius = 3;
catch_width = 10;
catch_height = 3;
catch_socket_width=catch_width + 1;
catch_socket_height=catch_height + 1;

case_x = 130 + (walls * 2);
case_y = 70 + (walls * 2);
case_z = 9 + walls;

depth = 6;

module fillet(r, h) {
    translate([r / 2, r / 2, 0])

        difference() {
            cube([r + 0.01, r + 0.01, h], center = true);

            translate([r/2, r/2, 0])
                cylinder(r = r, h = h + 1, center = true);

        }
}

module text3(txt) {
    translate([0,0,depth+walls+1]) {
        rotate([0,0,180]) {
            linear_extrude(1) {
                text(txt, valign = "top", size = 6, font = "Berlin Sans FB:style=Bold");
            }
        }
    }
}

module hexagon(w, h) {
    translate([0,0,h/2]) {
        hull() {
            cube([w / 1.7, w, h], center = true);
            rotate([0,0,120]) {
                cube([w / 1.7, w, h], center = true);
            }
        }
    }
}

module holes()
{
    spacing = 8.5;
    
    for (i = [0 : spacing : spacing * 14]) {
        translate([case_x - walls - 5 - i, walls + 12, depth]) {
            hexagon(hex, 5);
        }
    }
    
    for (i = [0 : spacing : spacing * 14]) {
        translate([case_x - walls - 5 - i, walls + 28.5, depth]) {
            hexagon(hex, 5);
        }
    }
    
    for (i = [0 : spacing : spacing * 9]) {
        translate([case_x - walls - 5 - i, walls + 45, depth]) {
            hexagon(hex, 5);
        }
    }
    
    for (i = [0 : spacing : spacing * 7]) {
        translate([case_x - walls - 5 - i, walls + 61.5, depth]) {
            hexagon(hex, 5);
        }
    }
    
    for (i = [0 : spacing : spacing * 2]) {
        translate([case_x - walls - 82 - i, walls + 61.5, depth]) {
            hexagon(hex, 5);
        }
    }
}

module handle()
{
    difference() {
        rotate([90,0,0]) {
            cylinder(47, 14, 14);
            translate() {
                cylinder(12, 5, 5);
            }
        }
        
        rotate([90,0,90]) {
            translate([-15,-35,-15]) {
                cylinder(30, 25, 25);
            }
        }
    }
}

module doText()
{
    sp = 16.5;
    
    translate([case_x - walls - 2,walls + (0 * sp) + 2,0])
        text3("FLATHEAD");
    translate([case_x - walls - 2,walls + (1 * sp) + 2,0])
        text3("PHILLIPS");
    translate([case_x - walls - 2,walls + (2 * sp) + 2,0])
        text3("POSI");
    translate([case_x - walls - 2,walls + (3 * sp) + 2,0])
        text3("TORX");
    translate([case_x - walls - 80,walls + (3 * sp) + 2,0])
        text3("HEX");
}

module top()
{
	union()
	{
		difference()
		{
            union() {
                translate([box_corner_radius, box_corner_radius, 0]){
                    minkowski() {
                        cube([case_x-(box_corner_radius*2),case_y-(box_corner_radius*2),case_z-1]);
                        cylinder(r=box_corner_radius,h=1);
                    }
               }

            }

			translate([walls, walls, walls])
			cube([case_x-(walls*2), case_y-(walls*2), case_z]);

			rotate([-90,-90,0])
                translate([0,0,case_y/2])
			        fillet(box_corner_radius,case_y);
			rotate([90,-90,0])
                translate([0,-case_x,-case_y/2])
			        fillet(box_corner_radius,case_y); 
			rotate([0,-90,0])
                translate([0,0,-case_x/2])
			        fillet(box_corner_radius,case_x); 
 		    rotate([180,-90,0])
                translate([0,-case_y,case_x/2])
			        fillet(box_corner_radius,case_x); 
		}
        
		//hinges
		for (i=[
			hinge_width+.5+box_corner_radius,
			case_x-(hinge_width*2)-1.5-box_corner_radius])
		{
			translate([i,0,case_z])
			rotate([-90,0,-90])
			hinge(hinge_width+1);
		}
        
        // add a catch
		if (catch_width != 0) {
		    difference() {
		        translate([(case_x-catch_width)/2,case_y-walls,case_z])
                    cube([catch_width,walls,catch_height]);
                union() {
		            translate([(case_x-catch_width)/2,case_y-walls/2,case_z])
                        cube([catch_width,walls/2,catch_height-1]);
		            translate([(case_x-catch_width)/2,case_y-walls/3,case_z])
                        cube([catch_width,walls/2,catch_height]);
                }
            }
		}
	}
}

module bottom()
{
	union()
	{
		difference()
		{
            union() {
                translate([box_corner_radius, box_corner_radius, 0]){
                    minkowski() {
                        cube([case_x-(box_corner_radius*2),case_y-(box_corner_radius*2),case_z-1]);
                        cylinder(r=box_corner_radius,h=1);
                    }
               }
            }
            
			translate([walls,walls,walls]) {
                translate([0,0,depth]) {
                    cube([case_x-(walls*2), case_y-(walls*2), 30]);
                }
            }
            
            holes();
            handle();

			rotate([-90,-90,0])
                translate([0,0,case_y/2])
			        fillet(box_corner_radius,case_y);
			rotate([90,-90,0])
                translate([0,-case_x,-case_y/2])
			        fillet(box_corner_radius,case_y); 
			rotate([0,-90,0])
                translate([0,0,-case_x/2])
			        fillet(box_corner_radius,case_x); 
 		    rotate([180,-90,0])
                translate([0,-case_y,case_x/2])
			        fillet(box_corner_radius,case_x);
            
		    // add the catch socket
		    if (catch_width != 0) {
		        translate([(case_x-catch_socket_width)/2,case_y-walls,case_z-catch_socket_height])
                        cube([catch_socket_width,walls/2,catch_socket_height]);
		        translate([(case_x-catch_socket_width)/2,case_y-walls,case_z-catch_socket_height])
                        cube([catch_socket_width,walls-1,2]);
		    }
		}
        
        doText();

		//hinges
		for (i=[
			box_corner_radius,
			(hinge_width*2)+2+box_corner_radius,
			case_x-(hinge_width*3)-2 - box_corner_radius,
			case_x-hinge_width -box_corner_radius])
		{
			translate([i,0,case_z])
			rotate([-90,0,-90])
			hinge();
		}
	}
}

module hinge(width=hinge_width, hole=2)
{
	difference()
	{
		union()
		{
			linear_extrude(height=width)
			polygon(points=[
				[-1,0],
				[hole+6,0],
				[0, case_z],
				[-1, case_z]
			]);

			translate([(hole+6)/2,0,0])
			cylinder(r=(hole+6)/2, h=width);
		}

		translate([(hole+6)/2,0,-1])
		cylinder(r=hole/2, h=width+2, $fn=12);
	}	
}

module assembled()
{
	translate([case_x,0,case_z*2]) {
        rotate([0,180,0]) {
            top();
        }
    }
	
	bottom();
}

//assembled();
$fn=50;

top();

rotate(a=[0,0,180]) {
    translate([-case_x,10,0]) {
        bottom();
    }
}
