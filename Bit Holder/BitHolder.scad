hex = 4.4;
clearance = 5;
spacing = 10;

difference() {
    cube([200,200,10]);
    translate([clearance, clearance]) {
        for (i = [0 : spacing : spacing * 15]) {
            translate([15,i + 5,3]) {
                hexagon (hex, 10);
            }
        }
    }
}

translate ([clearance, clearance]) {
    translate([0,0,9]) {
        text3("FLAT");
    }
}

module text3(txt) {
    rotate([0,0,90]) {
        translate([0,0,1]) {
            linear_extrude(1) {
                text(txt, valign = "top", size = 8, font = "Berlin Sans FB:style=Bold");
            }
        }
    }
}

module hexagon(w, h) {
    translate([0,0,h/2]) {
        hull() {
            cube([w / 1.7, w, h], center = true);
            rotate([0,0,120]) {
                cube([w / 1.7, w, h], center = true);
            }
        }
    }
}