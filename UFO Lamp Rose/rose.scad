$fn=60;

// Set values

mainDiameter = 90;
holeDistance = 55;
wallThickness = 2.5;
baseHeight = 15;
wireHoleDiameter = 25;

// Calculated Values

holeOffset = holeDistance / 2;

base();

translate([0,0,40]) lid();

module base()
{
    difference()
    {
        // Base
        cylinder(h=baseHeight, d=mainDiameter);
        translate([0,0,wallThickness])
            cylinder(h=baseHeight, d=mainDiameter - (wallThickness * 2));
    
    // Screw Holes
    translate([-holeOffset,0,-(23-wallThickness)+0.1]) screw(4, 10, 20, 3);
    translate([+holeOffset,0,-(23-wallThickness)+0.1]) screw(4, 10, 20, 3);
 
    // Main wire hole
    translate([0,0,-0.1])
        cylinder(h=wallThickness + 0.2, d=wireHoleDiameter);
 
    // Incoming wire holes
    translate([mainDiameter/2-5, 5, 6.5])
        rotate([0,90,0])
            cylinder(h=10, d=8);
    translate([mainDiameter/2-5, -5, 6.5])
        rotate([0,90,0])
            cylinder(h=10, d=8);
    }

    translate([-7.75,22,wallThickness])
        wago();

    rotate([0,0,140])
        translate([-7.75,22,wallThickness])
            wago();
    rotate([0,0,-140])
        translate([-7.75,22,wallThickness])
            wago();

    rotate([0,0,0])
        translate([0, -37.5, 0])
            screwPost();

    rotate([0,0,120])
        translate([0, -37.5, 0])
            screwPost();

    rotate([0,0,240])
        translate([0, -37.5, 0])
            screwPost();
}

module lid()
{
    difference()
    {
        union()
        {
            cylinder(h=2, d=mainDiameter);
            cylinder(h=10, d1=18, d2=12);
        }

        // Screw holes
        for (i = [0:1:2])
        {
            rotate([0, 0, 120 * i])
                translate([0, -37.5, -0.1])
                    cylinder(h=3, d=5);
        }
        
        // Cable hole
        cylinder(h=30, d=8);
    }
}

module screwPost()
{
    difference()
    {
        union()
        {
            cylinder(h=baseHeight, d=12);
            translate([-6, -6, 0])
                cube([12, 6, baseHeight]);
        }
        
        translate([0,0,2])
            cylinder(h=baseHeight, d=6);
    }
}

module screw(screwDia, headDia, screwHeight, headHeight)
{
    cylinder(h=screwHeight+0.01, d=screwDia);
    translate([0,0,screwHeight]) cylinder(h=headHeight, d1=screwDia, d2=headDia);
}

module wago()
{
    difference()
    {
        union()
        {
            cube([15.5, 20.5, 8.5]);
            translate([0, 8, 8.5])
                cube([15.5, 12.5, 0.6]);
        }
        translate([1,-.1,0])
            cube([13.5, 18.5, 8.51]);
    }
}