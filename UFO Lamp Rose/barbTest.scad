$fn=60;

thing();

module thing()
{
    difference()
    {
        cylinder(h=15, d=16);
        translate([0,0,-0.1])
            cylinder(h=15.2, d=8);
    }
    
    for (i=[0:4])
    {
        translate([0, 0, i * 3])
            barb(8, 0.5, 1);
    }
}

module barb(diameter, w, h)
{
    rotate_extrude(convexity = 10, $fn = 100)
        translate([diameter/2, 0, 0])
            polygon([
                [0, 0],
                [-w, 0],
                [0, h]
            ]);
}