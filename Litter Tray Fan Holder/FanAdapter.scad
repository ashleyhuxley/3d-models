include <threads.scad>;
include <modules.scad>;

$fn = 80;

ceiling();

module ceiling()
{
	difference()
	{
		union()
		{
			translate([0,0,33])
			{
				cylinder(h = 30, d1 = 54, d2 = 49);

				translate([0,0,6]) torus(25.5, 2);
				translate([0,0,16]) torus(24.8, 2);
				translate([0,0,26]) torus(23.8, 2);
			}

			translate([0,0,20])
				cylinder(h=13, d=44);

			translate([0,0,10])
				metric_thread(44, 1.5, 10, leadin=3);

			cylinder(h=10, d=35.4);
		}

		translate([0,0,-0.1])
			cylinder(h=63.2, d=33.4);

	}
}

module adapter()
{
	difference()
	{
		union()
		{
			rounded_rect(53, 53, 4, 5);
			translate([0, 0, 4])
			{
				cylinder(h = 30, d1 = 53, d2 = 48);
			}

			translate([0,0,10]) torus(24.5, 2);
			translate([0,0,20]) torus(23.8, 2);
			translate([0,0,30]) torus(22.8, 2);
		}
		translate([0,0,-0.1])
		{
			cylinder(h = 34.2, d = 38);

			translate([-16,-16,0])
				cylinder(h=9.2, d=4);
			translate([16,-16,0])
				cylinder(h=9.2, d=4);
			translate([-16,16,0])
				cylinder(h=9.2, d=4);
			translate([16,16,0])
				cylinder(h=9.2, d=4);
		}

		translate([13, -26.5, 1])
			cube([6, 13.75, 2.2]);
		translate([-19, -26.5, 1])
			cube([6, 13.75, 2.2]);
		translate([13, 26.5-13.75, 1])
			cube([6, 13.75, 2.2]);
		translate([-19, 26.5-13.75, 1])
			cube([6, 13.75, 2.2]);
	}
}

module torus(full_radius, profile_radius)
{
	rotate_extrude(convexity = 10)
		translate([full_radius, 0, 0])
			circle(r = profile_radius);
}