$fn=60;

difference()
{
    union()
    {
        cylinder(5, d=7);
        scale([1,1,0.5])
        {
            sphere(d=7);
        }
    }
    
    cylinder(5.1, d=6);
}
