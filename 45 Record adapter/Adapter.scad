fn = 60;

difference()
{
	union()
	{
		cylinder(h = 5, d = 38, $fn=120);
		translate([0,0,5])
		{
			cylinder(h = 8, d = 10, $fn=fn);
		}

		for (i = [0 : 1 : 2])
		{
			translate([0,0,8 + (i * 2)])
			{
				cylinder(h = 1, d = 10.5, $fn=fn);
			}
		}

		translate([-8,7,5])
		{
			linear_extrude(0.2)
			{
				text("Fen", size=7);
			}
		}
	}

	cylinder(h = 13.1, d = 7.3, $fn=fn);
}