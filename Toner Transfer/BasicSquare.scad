width = 80;
height = 80;

item();

module item()
{
    cube([width,height,0.8]);
}

module guides()
{
    guide();
    
    translate([width, 0, 0])
        rotate([0,0,90])
            guide();
    
    translate([width, height, 0])
        rotate([0,0,180])
            guide();
    
    translate([0, height, 0])
        rotate([0,0,270])
            guide();
}

module guide()
{
    translate([-10,-0.5,0])
        cube([20,0.5,0.2]);
    translate([-0.5,-10,0])
        cube([0.5,20,0.2]);
}