height = 2;
fn = 50;

donut(height, 80, 45);
translate([0,0,height])
{
    donut(2, 80, 56);
    donut(2, 53, 45);
}

module donut(h, od, id)
{
    difference()
    {
        cylinder(h, d = od, $fn=fn);
        cylinder(h, d = id, $fn=fn);
    }
}