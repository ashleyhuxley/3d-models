wall = 1.5;
width = 75;
height = 29;
depth = 15;

co_width = 8;
co_depth = 14;

full_width = width + (wall * 2);

difference()
{
    cube([depth, (width + wall + wall), (height + wall)]);
    
    translate([wall, wall, wall])
    {
        cube([depth, width, height + 0.1]);
    }
    
    translate([-0.1, wall + (full_width / 2), (height + wall) - co_depth])
    {
        translate([0, -(co_width / 2), 0])
        {
            cube([wall + 0.2, co_width, co_depth + 0.1]);
        }
        
        rotate([0,90,0])
        {
            cylinder(h = wall + 0.2, d = co_width, $fn=30);
        } 
    }
}

translate([0,-15,0]) wing(15);
translate([0,full_width,0]) wing(15);

module wing(size)
{
    thickness = 4;
    
    translate([0,0,(height + wall) - thickness])
    {
        difference()
        {
            cube([size, size, thickness]);
            translate([size/2, size/2, -0.1])
            {
                cylinder(h = 5 + 0.2, d = 4.3, $fn = 30);
                countersink(3, 8, 4.3);
            }
        }
    }
}



module countersink(depth, cd1, cd2)
{
    cylinder(h = depth, d1 = cd1, d2 = cd2, $fn=30);
}