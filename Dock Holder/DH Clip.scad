clip();

module clip()
{
    difference()
    {
        union()
        {
            translate([0,1,-4]) cube([30, 10, 4]);
            translate([20,0,0])
            {
                rotate([-90,0,90])
                {
                    linear_extrude(10)
                    {
                        polygon([[1,0], [1,29], [-1.5, 29], [2, 36], [3, 36], [5,0], [0,0]]);
                    }
                }
            }
            
            translate([15,4.5,-4])
            {
                rotate([0,90,0])
                {
                    fillet(2.5, 10);
                }
            }
        }
        
        translate([0,0,-4])
        {
            translate([5,5,-0.1]) 
            {
                cylinder(h = 4.2, d = 4.3, $fn = 30);
                countersink(3, 8, 4.3);
            }
            
            translate([25,5,-0.1])
            {
                cylinder(h = 4.2, d = 4.3, $fn = 30);
                countersink(3, 8, 4.3);
            }
        }
    }
}

module countersink(depth, cd1, cd2)
{
    cylinder(h = depth, d1 = cd1, d2 = cd2, $fn=30);
}

module fillet(r, h)
{
    translate([r / 2, r / 2, 0])

        difference() {
            cube([r + 0.01, r + 0.01, h], center = true);

            translate([r/2, r/2, 0])
                cylinder(r = r, h = h + 1, center = true, $fn = 30);

        }
}