difference()
{
    cube([42, 108, 5.5], center = true);
    
    mirror_copy(v = [0, 1, 0])
    {
        mirror_copy(v = [1, 0, 0])
        {
            translate([22, 10, -2.75])
            {
                rotate([0,0,20])
                {
                    cube([100, 100, 5.5]);
                }
            }
        }
    }

    translate([0,43,-2.75])
    {
        cylinder(5.5, d = 6, $fn = 30);
    }
    
    translate([0,-43,-2.75])
    {
        cylinder(5.5, d = 6, $fn = 30);
    }
}

module mirror_copy(v = [1, 0, 0]) {
    children();
    mirror(v) children();
}