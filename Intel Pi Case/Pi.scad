include <threads.scad>;

//screw();
//board();
post();

module board()
{
	difference()
	{
		union()
		{
			cube([56, 85, 2]);
			translate([3.5, 3.5, 2])
			{
				translate([0,0,0]) standoff();
				translate([49,0,0]) standoff();
				translate([49,58,0]) standoff();
				translate([0,58,0]) standoff();
			}
		}

		translate([3.5, 3.5, -0.1])
		{
			translate([0,0,0]) hole();
			translate([49,0,0]) hole();
			translate([49,58,0]) hole();
			translate([0,58,0]) hole();
		}

		translate([28,42.5,-0.01])
		{
			translate([0,0,1]) cylinder(h = 1.02, d=15, $fn=6);
			cylinder(h = 2.02, d=10.5, $fn=36);
		}
	}
}

module post()
{
	difference()
	{
		union()
		{
			translate([0,0,-35])
			{
				cylinder(h = 35, d1 = 16, d2=20, $fn=30);
				translate([0,0,-28])
				{
					cylinder(h = 35, d = 10.8, $fn=30);
				}
			}
		}
		
		translate([0,0,-12])
		{
			metric_thread(10.8, 2, 12, internal=true);
		}
	}
}

module screw()
{
	cylinder(h = 1, d = 14.5, $fn=6);
	metric_thread(10, 2, 10, internal=false, leadin=1);
}

module standoff()
{
	cylinder(h = 4, d = 6.5, $fn=30);
}

module hole()
{
	cylinder(h = 6.2, d = 2.8, $fn=30);
	translate([0,0,2]) cylinder(h = 6.2, d = 3.7, $fn=30);
}