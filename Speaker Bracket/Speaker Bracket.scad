w = 20;
h = 40;

wall = 1.5;
echo(wall);

drill = 3;
fn = 30;

difference()
{
    union()
        {
        difference()
        {
            linear_extrude(w)
            {
                polygon([[0,0],[h,0],[0,h],[0,0]]);
            }
                
            
            translate([wall,wall,wall])
            {
                cube([h, h, w - (wall * 2)]);
            }
        }

        translate([(h / 3) * 2, wall + 6, w / 2])
        {
            rotate([90,0,0])
            {
                cylinder(h = 6, d1 = 8, d2 = 8, $fn = fn);
            }
        }


        translate([wall, (h / 3) * 2, w / 2])
        {
            rotate([90,0,90])
            {
                cylinder(h = 6, d1 = 8, d2 = 8, $fn = fn);
            }
        }
    }
    
    translate([(h / 3) * 2, wall + 6, w / 2])
    {
        rotate([90,0,0])
        {
            cylinder(h = wall + 6, d1 = drill, d2 = drill, $fn = fn);
            cylinder(h = 2, d1 = 6, d2 = drill, $fn = fn);
        }
    }

    translate([0, (h / 3) * 2, w / 2])
    {
        rotate([90,0,90])
        {
            cylinder(h = wall + 6, d1 = drill, d2 = drill, $fn = fn);
            translate([0, 0, wall - 2 + 6])
            {
                cylinder(h = 2, d1 = drill, d2 = 6, $fn = fn);
            }
        }
    }
}