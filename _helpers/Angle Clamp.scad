use <threads.scad>




fn = 50;

difference()
{
    difference()
    {
        cylinder(10, d=40, $fn=fn);
        cylinder(10, d=10, $fn=fn);
    }

    translate([0,0,10])
    {
        for (i = [0:18:360])
        {
            rotate([0,0,i])
            {
                translate([0,15,0])
                {
                    rotate([0,45,0])
                    {
                        cube([2,10,2], center=true);
                    }
                }
            }
        }
    }
    
    metric_thread (12, 1.5, 10, internal = true, leadin = 1);
}