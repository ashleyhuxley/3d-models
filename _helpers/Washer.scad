od = 25;
height = 1;
id = 10;
fn = 50;

difference()
{
    cylinder(h = height, d = od, $fn=fn);
    translate([0,0,-0.1]) cylinder(h = height + .2, d = id, $fn=fn);
}