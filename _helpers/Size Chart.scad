thickness = 1.5;

difference()
{
    cube([250, 250, thickness]);
    translate([10, 10, -0.1])
    {
        for (y = [0: 0.2: 4.2])
        {
            translate([0, y * 56, 0]) cylinder(h = thickness + 0.2, d = y + 1, $fn=30);
        }
    }
    
    translate([50, 10, -0.1])
    {
        for (y = [0: 0.2: 4.2])
        {
            translate([0, y * 56, 0]) cylinder(h = thickness + 0.2, d = y + 5.2, $fn=30);
        }
    }
    
    translate([90, 10, -0.1])
    {
        for (y = [0: 0.2: 4.2])
        {
            translate([0, y * 56, 0]) cylinder(h = thickness + 0.2, d = y + 2, $fn=6);
        }
    }
    
    translate([125, 10, -0.1])
    {
        for (y = [0: 0.2: 4.2])
        {
            translate([0, y * 56, 0]) cylinder(h = thickness + 0.2, d = y + 6.2, $fn=6);
        }
    }
}

for (j = [0: 0.2: 4.2])
{
    translate([20, (j * 56) + 8, thickness])
    {
        color([1,0,0])
        linear_extrude(0.4)
        {
            text(text = str(j + 1), font = "Arial", size = 6);
        }
    }
}

for (j = [0: 0.2: 4.2])
{
    translate([60, (j * 56) + 8, thickness])
    {
        color([1,0,0])
        linear_extrude(0.4)
        {
            text(text = str(j + 5.2), font = "Arial", size = 6);
        }
    }
}

for (j = [0: 0.2: 4.2])
{
    translate([100, (j * 56) + 8, thickness])
    {
        color([1,0,0])
        linear_extrude(0.4)
        {
            text(text = str(j + 2), font = "Arial", size = 6);
        }
    }
}

for (j = [0: 0.2: 4.2])
{
    translate([135, (j * 56) + 8, thickness])
    {
        color([1,0,0])
        linear_extrude(0.4)
        {
            text(text = str(j + 6.2), font = "Arial", size = 6);
        }
    }
}

///

for (j = [0: 0.2: 4.2])
{
    translate([155, (j * 56) + 8, thickness])
    {
        cube([2, 11.2, 3]);
        translate([2.2 + j, 0, 0])
            cube([2, 11.2, 3]);
    }
}

for (j = [0: 0.2: 4.2])
{
    translate([165, (j * 56) + 8, thickness])
    {
        color([1,0,0])
        linear_extrude(0.4)
        {
            text(text = str(j + 0.2), font = "Arial", size = 6);
        }
    }
}

///

for (j = [0: 0.2: 4.2])
{
    translate([185, (j * 56) + 8, thickness])
    {
        cube([j * 0.2 + 0.2, 11.2, 3]);
    }
}

for (j = [0: 0.2: 4.2])
{
    translate([195, (j * 56) + 8, thickness])
    {
        color([1,0,0])
        linear_extrude(0.4)
        {
            text(text = str(j + 0.2), font = "Arial", size = 6);
        }
    }
}

///

for (j = [0: 0.2: 4.2])
{
    translate([215, (j * 56) + 8, thickness])
    {
        cube([j * 0.2 + 4.4, 11.2, 3]);
    }
}

for (j = [0: 0.2: 4.2])
{
    translate([225, (j * 56) + 8, thickness])
    {
        color([1,0,0])
        linear_extrude(0.4)
        {
            text(text = str(j + 4.4), font = "Arial", size = 6);
        }
    }
}
