difference()
{
    cube([55, 70, 2]);
    for (i = [1: 1: 6])
    {
        translate([5, (i * 10), 0])
        {
            dx = 5 + (i * 0.2);
            cylinder(h = 5, d = dx, $fn = 6);
            echo(dx);
        }
    }
    for (i = [1: 1: 6])
    {
        translate([15, (i * 10), 0])
        {
            dx = 6.2 + (i * 0.2);
            cylinder(h = 5, d = dx, $fn = 6);
            echo(dx);
        }
    }
    
    for (i = [1: 1: 6])
    {
        translate([25, (i * 10), 0])
        {
            dx = 7.4 + (i * 0.2);
            cylinder(h = 5, d = dx, $fn = 6);
            echo(dx);
        }
    }
    
    for (i = [1: 1: 6])
    {
        translate([35, (i *10), 0])
        {
            dx = 8.6 + (i * 0.2);
            cylinder(h = 5, d = dx, $fn = 6);
            echo(dx);
        }
    }

    for (i = [1: 1: 6])
    {
        translate([48, (i *10), 0])
        {
            dx = 9.8 + (i * 0.2);
            cylinder(h = 5, d = dx, $fn = 6);
            echo(dx);
        }
    }
}