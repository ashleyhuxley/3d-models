w = 162;
h = 45;
b = 3;

difference()
{
    cube([w,h,1]);
    translate([b,b,0.6])
    {
        cube([w-b-b, h-b-b, 1.1]);
    }
}

translate([b+b,b+b,0])
{
    translate([0,0,0])
    {
        linear_extrude(1)
        {
            text("2 E 0 V E E", 30, "Impact");
        }
    }
}