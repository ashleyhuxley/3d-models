$fn=60;

id = 19.8;
w = 12;
wall = 2;

od = id + wall + wall;

bracket();

module all()
{
    wall();

    translate([110,0,205])
    {
        rotate([-90,0,0])
        {
            translate([-80,0,0])
                pole();
            translate([80,0,0])
                pole();       
        }
    }

    translate([110,50,285])
    {
        rotate([-90,0,0])
            roll();
    }

    translate([0,30,219])
    {
        rotate([-90,0,0])
        {
            bracket();
        }
    }
}

module wall()
{
    color([0.72,0.81,1])
    {
        translate([-1,0,0])
            cube([1,1700,400]);
        translate([0,0,400])
            cube([400,1700,1]);
    }
}

module pole()
{
    color([0.72,1,0.81])
        cylinder(d=19, h=1220);
}

module roll()
{
    color([1,0.3,0.3])
    {
        cylinder(d=85, h=60);
    
        cylinder(d=200, h=3);
        translate([0,0,57])
            cylinder(d=200, h=3);
    }
}

module bracket()
{
    difference()
    {
        union()
        {
            linear_extrude(w)
            {
                polygon([
                    [0,0],
                    [203,0],
                    [211.84,21.34],
                    [21.34,211.84],
                    [0,203]
                ]);
            }
            
            translate([203,12.5,0])
            {
                cylinder(d=25, h=w);
            }
            translate([12.5,203,0])
            {
                cylinder(d=25, h=w);
            }           
        }
        
        translate([-0.1, 30, 6])
            rotate([90,180,90])
                keyhole_box(9,4.5,8,2.1,4,1);

        translate([-0.1, 180, 6])
            rotate([90,180,90])
                keyhole_box(9,4.5,8,2.1,4,1);
        
        translate([12,30,-0.1])
        {
            linear_extrude(w+0.2)
            {
                s = 177;
                polygon([
                    [5,0],
                    [s,0],
                    [s,5],
                    [5,s],
                    [0,s],
                    [0,5]
                ]);
            }
        }
        
        translate([50,8,-0.1])
        {
            cube([120, 15, w+0.2]);
        }

        translate([30,14,-0.1])
        {
            cylinder(d=id, h=w+0.2);
            translate([160,0,0])
                cylinder(d=id, h=w+0.2);
        }
    }
}

module keyhole(d1, d2, height, width)
{  
    cylinder(d=d1, h=width);
    translate([height,0,0])
        cylinder(d=d2, h=width);
    
    translate([0,-(d2/2),0])
        cube([height, d2, width]);
}

module keyhole_box(d1, d2, height, width, depth, offset)
{
    keyhole(d1, d2, height, width);
    
    translate([-(d1/2+offset), -(d1/2+offset), width])
    {
        cube([(d1/2)+(d2/2)+height+offset+offset,d1+offset+offset,depth]);
    }
}