use <threads.scad>

metric_thread (11, 1.5, 10, internal = false, leadin = 1);
translate([0,0,10])
{
    difference()
    {
        cylinder(6, d=15, $fn=30);

        for (i = [0:6:360])
        {
            rotate([0,0,i])
            {
                translate([0,8.6,3.5])
                {
                    rotate([90,0,45])
                    {
                        cube([2,7,2], center=true);
                    }
                }
            }
        }
    }
}