use <threads.scad>

height = 20;

difference()
{
    union()
    {
        // Main Cylinder
        cylinder(height, d=40, $fn=30);
        
        // Hose hanger
        translate([-5, -45, 0])
        {
            cube([10, 35, 10]);
        }
        
        translate([-5, -45, 10])
        {
            cube([10, 3, 5]);
        }
        
        translate([-5, -43.5, 15])
        {
            rotate([0,90,0])
            {
                cylinder(10, d=3, $fn=20);
            }
        }
        
        // Bum gun hanger
        width = 10;
        translate([-(width/2), 10, 0])
        {
            cube([width, 55, 10]);
        }
        
        translate([width/2, 19, 0])
        {
            cube([30, 10, 10]);
            translate([28, 10, 10])
            {
                rotate([90,0,0])
                {
                    cylinder(10, d=4, $fn=20);
                }
            }
        }
        
        translate([width/2, 55, 0])
        {
            translate([28, 10, 10])
            {
                rotate([90,0,0])
                {
                    cylinder(10, d=4, $fn=20);
                }
            }
            cube([30, 10, 10]);
        }
    }
    
    // Bar Cutout
    cylinder(height, d=26, $fn=30);
    
    translate([-5,-31,15])
    {
        rotate([0,90,0])
        {
            cylinder(10, d=22, $fn=30);
        }
    }
    
    // Thread Cutout
    rotate([0,0,90])
    {
        translate([0, -10, 10])
        {
            rotate([90,0,0])
            {
                metric_thread (12, 1.5, 10, internal = true, leadin = 1);
            }
        }
    }
}

