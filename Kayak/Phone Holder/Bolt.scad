use <threads.scad>

metric_thread (12, 1.5, 13, internal = false, leadin = 1);
translate([0,0,13])
	cylinder(h=5, d=18, $fn=8);