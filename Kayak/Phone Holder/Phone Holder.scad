use <threads.scad>

fn = 30;

difference()
{
    union()
    {
        rounded_rect(101,52, 20, 5.5);
        translate([50.5, 26, 20])
        {
            cylinder(30, d1 = 50, d2 = 25, $fn = fn);
            translate([0,0,30])
            {
                cylinder(20, d = 25, $fn=fn);
            }
            translate([0,15,40])
            {
                rotate([90,0,0])
                {
                    cylinder(5, d=15, $fn=fn);
                }
            }
        }
    }
    
    translate([8.5, 9.5, 0])
    {
        quads(84,33)
        {
            cylinder(20, d = 9, $fn=fn);
        }
        
        translate([0,0,14])
        {
            quads(84,33)
            {
                cylinder(6, d = 13, $fn=fn);
            }
        }
    }
    
    translate([50.5, 26, 0])
    {
        cylinder(100, d = 19.8, $fn=fn);
    }
    
    translate([50.5, 41, 60])
    {
        rotate([90,0,0])
        {
            metric_thread (13, 1.5, 10, internal = true, leadin = 1);
        }
    }
}

module rounded_rect(l, w, h, r)
{
    union()
    {
        translate([r    , 0    , 0]) cube([l - (r * 2), w          , h]);
        translate([0    , r    , 0]) cube([l          , w - (r * 2), h]);
        translate([r    , r    , 0]) cylinder(r = r, h, $fn=fn);
        translate([r    , w - r, 0]) cylinder(r = r, h, $fn=fn);
        translate([l - r, r    , 0]) cylinder(r = r, h, $fn=fn);
        translate([l - r, w - r, 0]) cylinder(r = r, h, $fn=fn);
    }
}

module quads(x, y)
{
    translate([0,0]) children();
    translate([x,0]) children();
    translate([0,y]) children();
    translate([x,y]) children();
}


