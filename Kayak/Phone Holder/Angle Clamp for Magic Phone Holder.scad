use <threads.scad>

fn = 50;
fa = 0.5;

//bolt();
//angle_clamp_m();
angle_clamp_f();

module angle_clamp_f()
{
    difference()
    {
        union()
        {
            union()
            {
                cylinder(10, d=40, $fn=fn);
                
                translate([4, 0, 1.4])
                {
                    rotate([0,90,0])
                    {
                        cylinder(h=14, d1 = 1, d2 = 16.5, $fa=fa);
                    }
                }
                
                translate([18, 0, 1.4])
                {
                    rotate([0,90,0])
                    {
                        cylinder(h=10, d=17, $fa=fa);
                    }

					difference()
					{
						translate([10,-15,-17/2])
						{
							cube([20,30,(17/2) - 0.5]);
						}

						translate([10 + 10, 0, -0.5])
						{
							sphere(7.8, $fn=fn);
							translate([0,-(8.5/2),-3])
								cube([11,8.5,5.5]);
						}
					}
                }
            }
        }
        
        translate([0,0,10])
        {
            for (i = [0:18:360])
            {
                rotate([0,0,i])
                {
                    translate([0,15,0])
                    {
                        rotate([0,45,0])
                        {
                            cube([2,10,2], center=true);
                        }
                    }
                }
            }
        }
        
        translate([0,0,8.5])
        {
            cylinder(1.5, d=20.5, $fa=fa);
        }    

        translate([0,0,0])
        {
            metric_thread (12, 1.5, 8.5, internal = true, leadin=1);
        }
    }
}

module bolt()
{
    //cylinder(7, d1=22, d2 = 14, $fa=fa);
    cylinder(4, d = 17.33, $fa=fa);
    translate([0,0,4])
    {
        cylinder(3, d1 = 17.33, d2 = 14, $fa=fa);
    }
    
    translate([0,0,7])
    {
        metric_thread (11, 1.5, 20, internal = false);
    }
}