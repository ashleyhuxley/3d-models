fn=80;

cylinder(h = 3, d = 35.5, $fn=fn);
translate([0,0,3])
{
	difference()
	{
		union()
		{
			cylinder(h = 23, d = 30, $fn=fn);
			for (i = [0 : 1 : 3])
			{
				translate([0,0,(i*5) + 3])
				{
					cylinder(h = 2, d=31, $fn=fn);
				}
			}
		}

		translate([0,0,1])
			cylinder(h = 23, d = 25, $fn=fn);
	}
}