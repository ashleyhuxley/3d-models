use <threads.scad>
use <modules.scad>

fn = 60;

metric_thread (16.5, 3.3, 9, square=true);
translate([0,0,9])
{
    cylinder(d = 16.5, 5.5, $fn=50);
}
translate([0,0,14.5])
{
    cylinder(d = 25, 3.5, $fn=50);
}

translate([0,2,22])
    rotate([90,0,0])
        rounded_rect(14, 25, 4, 4, fn);

translate([0,2,18])
rotate([90,0,0])
	rotate([0,90,0])
		fillet(2, 23.5, fn);

translate([0,-2,18])
rotate([-90,0,0])
	rotate([180,90,0])
		fillet(2, 23.5, fn);