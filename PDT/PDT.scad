$fn = 60;

translate([0,0,-6])
{
    difference()
    {
        cylinder(h=12, d=20, $fn=120);
        translate([-9.2,-3,11.8])
        {
            linear_extrude(1.1)
            {
                text("PDT", size=6.5);
            }
        }
    }
}

rotate([90,0,0])
{
    cylinder(h=35, d=6);
}

rotate([0,90,0])
{
    cylinder(h=35, d=8);
}

rotate([-90,0,0])
{
    cylinder(h=35, d=10);
}

rotate([0,-90,0])
{
    cylinder(h=35, d=12);
}