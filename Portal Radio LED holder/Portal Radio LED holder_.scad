$fn=20;

thickness = 2;
thicknessc = thickness + 0.2;

difference()
{
    // Start with an oval
    linear_extrude(thickness)
        resize([290, 195])
            circle($fn=60);
    
    // Cut it in half
    translate([-150,-100,-0.1])
        cube([150,200,thicknessc]);

    
    // LEDs 
    translate([26.4, 85.8, -0.1])   cylinder(d=5.4, h=thicknessc);
    translate([51.6, 80.8, -0.1])   cylinder(d=5.4, h=thicknessc);
    translate([75, 72.8, -0.1])     cylinder(d=5.4, h=thicknessc);
    translate([95.5, 62, -0.1])     cylinder(d=5.4, h=thicknessc);
    translate([112.3, 48.7, -0.1])  cylinder(d=5.4, h=thicknessc);
    translate([124.6, 33.7, -0.1])  cylinder(d=5.4, h=thicknessc);
    translate([132.5, 17, -0.1])    cylinder(d=5.4, h=thicknessc);
    // Mid point
    translate([135, 0, -0.1])       cylinder(d=5.4, h=thicknessc);
    translate([26.4, -85.8, -0.1])  cylinder(d=5.4, h=thicknessc);
    translate([51.6, -80.8, -0.1])  cylinder(d=5.4, h=thicknessc);
    translate([75, -72.8, -0.1])    cylinder(d=5.4, h=thicknessc);
    translate([95.5, -62, -0.1])    cylinder(d=5.4, h=thicknessc);
    translate([112.3, -48.7, -0.1]) cylinder(d=5.4, h=thicknessc);
    translate([124.6, -33.7, -0.1]) cylinder(d=5.4, h=thicknessc);
    translate([132.5, -17, -0.1])   cylinder(d=5.4, h=thicknessc);
    
    translate([30,10,-0.1])
        linear_extrude(thicknessc)
            polygon([[0,0], [90,0], [60,35], [0,60]]);
            
    translate([30,-10,-0.1])
        linear_extrude(thicknessc)
            polygon([[0,0], [90,0], [60,-35], [0,-60]]);
}

translate([0,50,thickness])
{
    difference()
    {
        rotate([90,0,0])
        {
            linear_extrude(100)
            {
                polygon([[0,0], [25,0], [0,25]]);
            }
        }
        
        translate([2,-98,0])
            cube([25, 96, 25]);
        
        translate([-0.1, -80, 12])
            rotate([0,90,0])
                cylinder(d=3, h=5);

        translate([-0.1, -20, 12])
            rotate([0,90,0])
                cylinder(d=3, h=5);       
    }
    
}