spacing = 40;

l = spacing * 6;
w = spacing * 3;
h = 8;

depth = h + 25;

wall = 4;

fn = 50;

m6 = 6.8;


module holder(d1, d2) {
    difference() {
        union() {
            cylinder(h + 2, d = d1 + 4, $fn = fn);
            cylinder(depth, d = d2 + 4, $fn = fn);
        }
    
        cylinder(h, d = d1 + 0.2, $fn = fn);
        cylinder(depth, d = d2 + 0.2, $fn = fn);
    }
}

module bits() {
    bw = 52.4;
    bl = 10.2;
    
    difference() {
        cube([bw + 4, bl + 4, 12.5], center = true);
        translate([0,0,-2]) {
            cube([bw, bl, 10.5], center = true);
        }
    }
}

module lug() {
    difference() {
        cube([15, 4, 15], center = true);
    }
}

module bitHolders() {
    difference() {
        union() {

            // Struts
            translate([0,0,h/2]) {
                for (a = [1 : 1 : 6]) {
                    translate([a * spacing, (a / 2 == round(a/2) ? spacing * 2 : spacing)]) {
                        rotate([0,0,45]) {
                            cube([4, 27, h], center = true);
                        }
                    }
                    translate([a * spacing, (a / 2 == round(a/2) ? spacing : spacing * 2)]) {
                        rotate([0,0,135]) {
                            cube([4, 27, h], center = true);
                        }
                    }
                }
                
                translate([spacing * 2.5, spacing * 0.5]) {
                    cube([51, 4, h], center = true);
                }
                translate([spacing * 4.5, spacing * 0.5]) {
                    cube([51, 4, h], center = true);
                }
                translate([spacing * 2.5, spacing * 2.5]) {
                    cube([51, 4, h], center = true);
                }
                translate([spacing * 4.5, spacing * 2.5]) {
                    cube([51, 4, h], center = true);
                }
                translate([spacing * 1.5, spacing * 1.5]) {
                    cube([51, 4, h], center = true);
                }
                translate([spacing * 3.5, spacing * 1.5]) {
                    cube([51, 4, h], center = true);
                }
                translate([spacing * 5.5, spacing * 1.5]) {
                    cube([51, 4, h], center = true);
                }
                
                
                translate([spacing * 1.5, spacing * 1.5]) {
                    cube([4, 50, h], center = true);
                }
                translate([spacing * 3.5, spacing * 1.5]) {
                    cube([4, 50, h], center = true);
                }
                translate([spacing * 5.5, spacing * 1.5]) {
                    cube([4, 50, h], center = true);
                }
            }

            // Bit Holders
            translate([0,0,6]) {
                translate([spacing * 2.5, 5]) {
                    bits();
                }
                
                translate([spacing * 4.5, 5]) {
                    bits();
                }
            }
           
            // Driver Holders
            translate([spacing / 2, spacing / 2]) {
                translate([spacing, spacing * 2]) {
                    holder(31, 12);
                }
                translate([0, spacing]) {
                    holder(29, 10);
                }
                translate([spacing, 0]) {
                    holder(27, 9);
                }
                translate([spacing * 2, spacing]) {
                    holder(27, 7);
                }
                translate([spacing * 3, 0]) {
                    holder(27, 6.5);
                }
                translate([spacing * 4, spacing]) {
                    holder(27, 5);
                }
                translate([spacing * 3, spacing * 2]) {
                    holder(26, 6.5);
                }
                translate([spacing * 5, 0]) {
                    holder(27, 5);
                }
                translate([spacing * 5, spacing * 2]) {
                    holder(29, 6.5);
                }
                translate([spacing * 6, spacing]) {
                    holder(28, 22);
                }
            }
        }
        
        // Hack for biggest screwdiver
        translate([spacing * 1.5, spacing * 2.5]) {
            cylinder(h, d = 31.2, $fn = fn);
        }      
    }

    // Lugs for fixings
    translate([19,83,7.5]) {
        rotate([0,0,90]) {
            lug();
        }
    }
    
    translate([260,83,7.5]) {
        rotate([0,0,90]) {
            lug();
        }
    }
    
    translate([spacing * 2.5, spacing * 2.5, 7.5]) {
        lug();
    }
    
    translate([spacing * 4.5, spacing * 2.5, 7.5]) {
        lug();
    }
}

module rack() {
    translate([12.8, 120, 0]) {
        cube([249,3,15]);
    }
    
    // Left & right arms
    translate([253.8, 76.7, 0]) {
        difference() {
            cube([8,44,15]);
            translate([4,0,0]) {
                cube([4,14.3,15]);
            }
        }
    }
    translate([12.8, 76.7, 0]) {
        difference() {
            cube([8,44,15]);
            translate([4,0,0]) {
                cube([4,14.3,15]);
            }
        }
    }
    
    // Screw lug & fillets R
    translate([92.5,102.3, 0]) {
        translate([7.5,20.7,-10])
        {
            difference() {
                union() {
                    translate([0,-1.5,12.5]) {
                        cube([23,3,23], center = true);
                    }
                    rotate([90,0,0]) {
                        cylinder(3, d = 23);
                    }
                }
                rotate([90,0,0]) {
                    cylinder(3, d1 = 5.3, d2 = 10);
                }
            }
        }
        translate([0,17.7,7.5]) {
            rotate([0,0,180]) {
                fillet(3, 15);
            }
        }
        translate([15,17.7,7.5]) {
            rotate([180,0,0]) {
                fillet(3, 15);
            }
        }
        difference() {
            cube([15,20,15]);
            translate([2, 2, 0]) {
                cube([11, 3.5, 13.3]);
            }
        }
    }
    
    // Screw lug & fillets L
    translate([172.5,102.3, 0]) {
        translate([7.5,20.7,-10])
        {
            difference() {
                union() {
                    translate([0,-1.5,12.5]) {
                        cube([23,3,23], center = true);
                    }
                    rotate([90,0,0]) {
                        cylinder(3, d = 23);
                    }
                }
                rotate([90,0,0]) {
                    cylinder(3, d1 = 5.3, d2 = 10);
                }
            }
        }
        translate([0,17.7,7.5]) {
            rotate([0,0,180]) {
                fillet(3, 15);
            }
        }
        translate([15,17.7,7.5]) {
            rotate([180,0,0]) {
                fillet(3, 15);
            }
        }
        difference() {
            cube([15,20,15]);
            translate([2, 2, 0]) {
                cube([11, 3.5, 13.3]);
            }
        }
    }
    
    // Small fillets
    translate([111.5,121.5,0]) {
        rotate([270,0,0]) {
            fillet(3, 3);
        }
    }
    translate([191.5,121.5,0]) {
        rotate([270,0,0]) {
            fillet(3, 3);
        }
    }
    
    translate([88.5,121.5,0]) {
        rotate([90,180,0]) {
            fillet(3, 3);
        }
    }
    translate([168.5,121.5,0]) {
        rotate([90,180,0]) {
            fillet(3, 3);
        }
    }
    
    // Small end fillets
    translate([27.8,121.5,15]) {
        rotate([90,0,0]) {
            fillet(3, 3);
        }
    }
    
    translate([246.8,121.5,15]) {
        rotate([90,0,180]) {
            fillet(3, 3);
        }
    }
    
    // Corner fillets
    translate([253.8,120,7.5]) {
        rotate([0,0,180]) {
            fillet(3, 15);
        }
    }
    translate([20.7,120,7.5]) {
        rotate([180,0,0]) {
            fillet(3, 15);
        }
    }
    
    // End Struts
    translate([246.8, 120, 0]) {
        bracket(7);
    }
    translate([12.8, 120, 0]) {
        bracket(0);
    }   
}

module bracket(offset) {
    cube([15, 3, 60]);
    translate([7.5,3,65]) {
        rotate([90,0,0]) {
            difference() {
                cylinder(3, d = 25);
                cylinder(3, d1 = 5.3, d2 = 10);
            }
        }
    }
    translate([offset,3,40]) {
        rotate([135,0,0]) {
            cube([8, 4, 40]);
        }
    }
}

module fillet(r, h) {
    translate([r / 2, r / 2, 0])

        difference() {
            cube([r + 0.01, r + 0.01, h], center = true);

            translate([r/2, r/2, 0])
                cylinder(r = r, h = h + 1, center = true, $fn = fn);

        }
}

module boltHoles() {
    translate([12,85,8]) {
        rotate([0,90,0]) {
            cylinder(10, d = m6, $fn = fn);
        }
    }
    
    translate([253,85,8]) {
        rotate([0,90,0]) {
            cylinder(10, d = m6, $fn = fn);
        }
    }
    
    translate([100,117,8]) {
        rotate([90,0,0]) {
            cylinder(20, d = m6, $fn = fn);
        }
    }
    
    translate([180,117,8]) {
        rotate([90,0,0]) {
            cylinder(20, d = m6, $fn = fn);
        }
    }
}

rotate([0,180,0]) {

    difference() {
        union() {
            bitHolders();
            //rack();
        }
        boltHoles();
    }
    
}


