wall = 5;
clearance = 2;

phone_width = 78;
phone_depth = 19;
phone_height = 153;

width = phone_width + (clearance * 2) + (wall * 2);
depth = phone_depth + wall + clearance;

co_width = width - (wall * 2);
co_depth = depth - wall;

difference()
{
	cube([width, depth, phone_height * 0.8]);

	translate([wall, -0.1, wall])
	{
		cube([co_width, co_depth + 0.1, phone_height]);
	}

	translate([wall + 23, co_depth / 2, -0.1])
	{
		cylinder(h = wall + 0.2, d = 13.5, $fn=60);
	}
}

translate([0,-wall,0])
{
	cube([width, wall, 15]);
}