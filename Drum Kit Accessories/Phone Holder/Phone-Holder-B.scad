mount();

module thing()
{
	difference()
	{
		holder();
		translate([25, 26.2, 50])
			rotate([90, 90, 0])
				mount();

		translate([11,26.5,55])
			rotate([90,0,0])
				holes(12, 3, 2.5, 6);
	}
}

module holes(s, cx, cy, h)
{
	intersection()
	{
		cube([cx * s * 2, cy * s * 2, h]);

		translate([0, -sqrt((s*s)+(s*s)) / 2, 0])
		{
			for (i = [0:1:cx])
				for (j = [0:1:cy])
					translate([i * (s * 2), j * (s * 2), 0])
						rotate([0,0,45])
							cube([s, s, h]);

			for (i = [0:1:cx])
				for (j = [0:1:cy])
					translate([s + (i * (s * 2)), s + (j * (s * 2)), 0])
						rotate([0,0,45])
							cube([s, s, h]);
		}
	}
}


module holder()
{
	difference()
	{
		import("phoneholder-b.stl");

		for (i = [1 : 1 : 6])
			translate([-0.1, 12, (i * 18) + 5])
				rotate([0,90,0])
					cylinder(d = 12, h = 120, $fn=30);
	}
}

module mount()
{
	cube([24, 40, 1]);

	translate([0,0,1])
	{
		translate([8, 10, 0])
			cylinder(h = 4, d = 4.7, $fn=30);
		translate([8, 20, 0])
			cylinder(h = 4, d = 4.7, $fn=30);
		translate([8, 30, 0])
			cylinder(h = 4, d = 4.7, $fn=30);
		translate([16, 10, 0])
			cylinder(h = 4, d = 4.7, $fn=30);
		translate([16, 20, 0])
			cylinder(h = 4, d = 4.7, $fn=30);
		translate([16, 30, 0])
			cylinder(h = 4, d = 4.7, $fn=30);
	}
}