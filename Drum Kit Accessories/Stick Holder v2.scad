offset = 12;
dia = 30;
wall = 3;
height = 150;

fn = 30;

outer_dia = dia + wall + wall;
outer_radius = outer_dia / 2;

difference()
{
    union()
    {
        translate([-offset,0,0])
        {
            cylinder(h = height, d = outer_dia, $fn=fn);
        }
        translate([offset,0,0])
        {
            cylinder(h = height, d = outer_dia, $fn=fn);
        }
        
        translate([-20,outer_radius + 2.8,100])
            rotate([90,0,0])
                bracket();
    }

    translate([-offset,0,0])
    {
        holes();
        translate([0,0,wall]) cylinder(h = height, d = dia, $fn=fn);
    }
    
    translate([offset,0,0])
    {
        holes();
        translate([0,0,wall]) cylinder(h = height, d = dia, $fn=fn);
    }
}

module holes()
{
    translate([0,0,10])
    {
        rotate([90,0,0])
        {
            for (i = [0 : 1 : 21])
            {
                translate([0,i * 6,0])
                {
                    angle = i * 45;
                    for (j = [0 : 1 : 3])
                    {
                        rotate([0,angle + (90 * j),0])
                            cylinder(h = outer_radius, d = 9, $fn = 30);
                    }
                }
            }
        }
    }
}

module bracket()
{
    difference()
    {
        cube([40, 24, 3]);
        translate([10,12,0]) cylinder(h = 3, d = 4.6, $fn=30);
        translate([30,12,0]) cylinder(h = 3, d = 4.6, $fn=30);
    }
}