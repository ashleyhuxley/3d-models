width = 40;
wall = 4;
arm_dia = 40;
cutout_width = 5;

tabWidth = 8;
tabLength = 20;

plateWidth = 24;
plateThickness = 5;

fn = 30;

difference()
{

    union()
    {
        difference()
        {
            cylinder(h = width, d = arm_dia + (wall * 2), $fn=fn);
            translate([0,0,-0.1])
            {
                cylinder(h = width + 0.2, d = arm_dia, $fn=fn);
                translate([0,-cutout_width/2,0])
                {
                    cube([(arm_dia / 2) + wall, cutout_width, width + 0.2]);
                }
            }
        }
        
        // plate
        translate([-arm_dia / 2 - plateThickness, 0, 0])
        {
            translate([0, -plateWidth / 2, 0])
            {
				union()
				{
					cube([plateThickness, plateWidth, width]);
            
            
					rotate([90,0,90])
						translate([0,0,-3.8])
						{
							translate([8, 10, 0])
								cylinder(h = 3.8, d = 4, $fn=30);
							translate([8, 20, 0])
								cylinder(h = 3.8, d = 4, $fn=30);
							translate([8, 30, 0])
								cylinder(h = 3.8, d = 4, $fn=30);
							translate([16, 10, 0])
								cylinder(h = 3.8, d = 4, $fn=30);
							translate([16, 20, 0])
								cylinder(h = 3.8, d = 4, $fn=30);
							translate([16, 30, 0])
								cylinder(h = 3.8, d = 4, $fn=30);
						}
				}
			}
        }

        // tabs
        translate([(arm_dia / 2), cutout_width / 2, 0]) cube([tabLength, 8, width]);
        translate([(arm_dia / 2), -cutout_width / 2 - tabWidth, 0]) cube([tabLength, tabWidth, width]);
    }

    // Screw hole(s)
    translate([(arm_dia / 2) + (tabLength / 2), 0, (width - 10)])
    {
        rotate([90,0,0])
        {
            translate([0, 0, -(cutout_width / 2) - tabWidth - 0.1]) cylinder(h = tabWidth + 0.2, d = 5.5, $fn=fn);
            translate([0, 0, (cutout_width / 2) - 0.1])                   cylinder(h = tabWidth + 0.2, d = 6  , $fn=fn);
        }
    }
    
    translate([(arm_dia / 2) + (tabLength / 2),0, 10])
    {
        rotate([90,0,0])
        {
            translate([0, 0, -(cutout_width / 2) - tabWidth - 0.1]) cylinder(h = tabWidth + 0.2, d = 5.5, $fn=fn);
            translate([0, 0, (cutout_width / 2) - 0.1])                   cylinder(h = tabWidth + 0.2, d = 6  , $fn=fn);
        }
    }
}