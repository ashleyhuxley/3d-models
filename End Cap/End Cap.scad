inner_diameter = 12.5;
wall = 2;
height = 12;
bottom_wall = 7;

fn = 30;

outer_diameter = inner_diameter + (wall * 2);
outer_radius = outer_diameter / 2;

difference()
{
    union()
    {
        cylinder(d = outer_diameter, h = height, $fn=fn);
        scale([1,1,0.5])
        {
            sphere(d = outer_diameter, $fn=fn);
        }
    }
    
    cylinder(h = height, d1 = inner_diameter, d2 = inner_diameter + 0.6);
    
}