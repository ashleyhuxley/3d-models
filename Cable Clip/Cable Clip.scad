height = 18;
inner_width = 10;
inner_length = 30;

cutout_width = 8;

wall = 3.6;
hole_offset = 5;

fn = 20;

outer_length = inner_length + (wall * 2);
outer_width = inner_width + (wall * 2);

clip();

module clip()
{
    r = 2;
    
    adj = outer_length - cutout_width;  
    angle = atan(height / adj);
    
    h1 = hole_offset;
    h2 = height - hole_offset;
    
    adj1 = h1 / tan(angle);
    adj2 = h2 / tan(angle);
    
    difference()
    {
        stadium(outer_length, outer_width, height);
        translate([0, wall, 0])
        {
            stadium(outer_length, inner_width, height);
        }
        
        translate([0, wall + 1, 0])
        {
            rotate([90,0,0])
            {
                linear_extrude(wall + 1)
                {
                    polygon([
                        [outer_length - cutout_width, 0], 
                        [0, height], 
                        [cutout_width, height],
                        [outer_length, 0], 
                        [outer_length - cutout_width, 0]
                    ]);
                }
            }
        }
        
        translate([adj - adj1 + (cutout_width / 2),outer_width - wall,h1])
        {
            rotate([-90, 0, 0])
            {
                countersink(4.3, 8, wall, 3);
            }
        }
        
        translate([adj - adj2 + (cutout_width / 2),outer_width - wall,h2])
        {
            rotate([-90, 0, 0])
            {
                countersink(4.3, 8, wall, 3);
            }
        }
    }
    
    //translate([adj - r, wall, r / 2])
        //rotate([90,0,0])
            //cylinder(h = wall, d1 = r, d2 = r, $fn = fn);
    
    //translate([cutout_width + r, wall, height - (r / 2)])
        //rotate([90,0,0])
            //cylinder(h = wall, d1 = r, d2 = r, $fn = fn);

}

module countersink(cd1, cd2, l, cl)
{
    cylinder(h = l, d1 = cd1, d2 = cd1, $fn=fn);
    cylinder(h = cl, d1 = cd2, d2 = cd1, $fn=fn);
}

module stadium(l, w, h)
{
    cube([l, w, h]);
    translate([0,w / 2, 0])
    {
        cylinder(h = h, d1 = w, d2 = w, $fn = fn);
        translate([l, 0, 0])
        {
            cylinder(h = h, d1 = w, d2 = w, $fn = fn);
        }
    }
}