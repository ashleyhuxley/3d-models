$fn = 60;

//base();

translate([0,0,25]) foot();

module base()
{
	cylinder(h = 2, d = 40);
	for (i = [0 : 1 : 4])
	{
		rotate([0, 0, 90 * i])
			translate([10, 0, 2])
				cylinder(h = 2, d = 3.4);
	}
}

module foot()
{
	difference()
	{
		union()
		{
			cylinder(h = 30, d = 25);
			cylinder(h = 5, d = 40);
			translate([0,0,5]) cylinder(h = 10, d1=40, d2=25);
		}

		translate([0,0,5]) cylinder(h = 30, d=19.5);

		for (i = [0 : 1 : 4])
		{
			rotate([0, 0, 90 * i])
				translate([10, 0, -0.1])
					cylinder(h = 3, d = 4.2);
		}
	}
}