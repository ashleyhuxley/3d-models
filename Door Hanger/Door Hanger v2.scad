wallTop = 1;
wallSide = 3;
width = 10;
door = 34.5;
height = 35;

difference() {
    union() {
        cube([width, door + (wallSide * 2), height]);
        translate([0, -1.5, 0]) {
            rotate([0, 90, 0]) {
                difference() {
                    union() {
                        cylinder(width, 4, 4, $fn=20);
                        translate([3,0,0]) {
                            cylinder(width, 4, 4, $fn=20);
                        }
                    }
                    
                    for (x = [0 : 1 : 6] ) {
                        translate([(x * 1.7) - 1,0,0]) {
                            cylinder(width, 1.5 - (x * 0.12), 1.5 - (x * 0.12), $fn=20);
                        }
                    }
                }
            }
        }
    }

    translate([0, wallSide, 0]) {
        cube([width, door, height - wallTop]);
    }
}