wallTop = 1;
wallSide = 3;
width = 20;
door = 34.5;
height = 35;

difference() {
    union() {
        cube([width, door + (wallSide * 2), height]);
        translate([0, -2, 3]) {
            rotate([0, 90, 0]) {
                difference() {
                    cylinder(width, 5, 5, $fn=20);
                    cylinder(width, 3, 3, $fn=20);
                    translate([-6, 0, 0]) {
                        cube([5, 2, width]);
                    }
                }
            }
        }
    }

    translate([0, wallSide, 0]) {
        cube([width, door, height - wallTop]);
    }
}