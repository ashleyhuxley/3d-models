thickness = 5;
hinge = 2;
hingel = thickness + (2 * hinge);

linear_extrude(thickness)
{
    polygon(points = [[0,0], [-25,13], [-25,15], [20,15], [20,15], [20,10], [0,10], [0,0]]);
}


translate([15, 13, -hinge])
{
    cylinder(hingel, d = 4, $fn=30);
}