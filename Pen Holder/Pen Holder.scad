thick = 7;
clear_depth = 5;

h = 12;

fn = 40;

carriage();
translate([35,7,9])
{
    pen(9);
    translate([0,0,21])
    {
        clip(9);
    }
}


module stadium(dia, height, width)
{
    hull()
    {
        translate([(width/2),0,0]) cylinder(h = height, d = dia, $fn=fn);
        translate([-(width/2),0,0]) cylinder(h = height, d = dia, $fn=fn);
    }
}

module clip(dia)
{
    difference()
    {
        cube([29.2, h, 10]);
        rotate([90,0,0])
        {
            translate([14.6,0,-(h+.1)])
            {
                cylinder(h = h + .2, d = dia, $fn=fn);
            }
        }
        
        translate([24.2, h/2, -0.1])
        {
            cylinder(h = 10.2, d = 5, $fn=fn);
        }
        
        translate([5, h/2, -0.1])
        {
            cylinder(h = 10.2, d = 5, $fn=fn);
        }
    }
}

module pen(dia)
{   
    difference()
    {
        union()
        {
            cube([29.2, h, 19]);
            translate([14.6, h, 0])
            {
                cylinder(h = 10, d = 14);
                translate([-7,0,0]) cube([14, 38, 5]);
                
                for (i = [1 : 5])
                {
                    translate([0,6 + (i * 6),5]) stadium(4, 5, 6);
                }
            }
        }
        
        translate([24.2, h/2, 19 - 9])
        {
            cylinder(h = 9.2, d = 5.7, $fn=fn);
        }
        
        translate([5, h/2, 19 - 9])
        {
            cylinder(h = 9.2, d = 5.7, $fn=fn);
        }
        
        rotate([90,0,0])
        {
            translate([5, 7, -(h + .1)])
            {
                cylinder(h = h + .2, d = 4.8, $fn=fn);
            }
            translate([24.2, 7, -(h + .1)])
            {
                cylinder(h = h + .2, d = 4.8, $fn=fn);
            }
            
            translate([14.6, 20, -(h + .1)])
            {
                cylinder(h = h + .2, d = dia, $fn=fn);
            }
        }
    }
}

module carriage()
{
    difference()
    {
        cube([64.2, 46.1, thick]);
        translate([0, 46.1, 0])
        {
            translate([12,-12,0])
            {
                cylinder(h = clear_depth, d = 12, $fn=fn);
            }
            
            translate([12 + 40.2,-12,0])
            {
                cylinder(h = clear_depth, d = 12, $fn=fn);
            }
            
            translate([12.75, -(14.3 + 19), 0])
            {
                cylinder(h = thick + 0.1, d = 3.4, $fn=fn);
            }
            
            translate([12.75 + 17.75, -14.3, 0])
            {
                cylinder(h = thick + 0.1, d = 3.4, $fn=fn);
            }
            
            translate([12.75 + 17.75 + 10.9, -24.85, 0])
            {
                cylinder(h = clear_depth, d = 5.7, $fn=fn);
            }
            
            translate([12.75 + 17.75 + 10.9 + 14, -24.85, 0])
            {
                cylinder(h = clear_depth, d = 5.7, $fn=fn);
            }
        }
    }

    translate([35,0,thick])
    {
        difference()
        {
            cube([29.2, 6, 14]);
            rotate([90,0,0])
            {
                translate([5, 9, -6.1])
                {
                    cylinder(h = 5, d = 3.8, $fn=fn);
                }
                translate([24.2, 9, -6.1])
                {
                    cylinder(h = 5, d = 3.8, $fn=fn);
                }
            }
        }
        
        rotate([0,90,0])
        {
            translate([-9,3,-4])
            {
                stadium(3, 4, 4);
            }
            translate([-9,3,29.2])
            {
                stadium(3, 4, 4);
            }
        }
    }
}