fn=60;


union()
{
    difference()
    {
        cylinder(h = 12, d = 8.5, $fn=fn);
        cylinder(h = 10, d = 6.2, $fn=fn);
    }
    
    rotate([0, 0,   0]) translate([2, -0.3, 0]) cube([1.5, 0.6, 10]);
    rotate([0, 0,  90]) translate([2, -0.3, 0]) cube([1.5, 0.6, 10]);
    rotate([0, 0, -90]) translate([2, -0.3, 0]) cube([1.5, 0.6, 10]);
    
    translate([0,0,12])
    {
        cylinder(h = 4, d1 = 8.5, d2 = 4, $fn=fn);
    }
    
    difference()
    {
        rotate([0,0,90])
        {
            hull()
            {
                translate([5,2,19])
                {
                    rotate([90,0,0])
                    {
                        cylinder(h = 4, d = 14, $fn=fn);
                    }
                }
                
                translate([-5,2,19])
                {
                    rotate([90,0,0])
                    {
                        cylinder(h = 4, d = 14, $fn=fn);
                    }
                }
            }
        }
        
        rotate([0,0,90])
        {
            translate([-6,2,19])
            {
                rotate([90,0,0])
                {
                    cylinder(h = 4, d = 3, $fn=fn);
                }
            }
        }
    }
    
    translate([0, 10, 23])
    {
        sphere(d = 3, $fn=20);
    }
}