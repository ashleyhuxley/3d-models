include <modules.scad>

$fn=30;

//cube([20,30,1]);
translate([10, 15, 0])
    rounded_rect(30, 20, 1, 1);
translate([5.5,15,1])
{
    difference()
    {
        cube([9, 3, 1]);
        translate([0,3,0.5])
            rotate([0,90,0])
                cylinder(h=9, d=1);
    }
    
    translate([4.5,0,0])
        rotate([90, 0, -90])
            fillet(1, 9);
    
    translate([0,0,1])
    {
        cube([9, 12, 1]);
        translate([0, 12, 0.5])
        {
            rotate([0,90,0])
                cylinder(h = 9, d = 1);
        }
    }
}

