$fn=60;

width = 15;

difference()
{
    cube([2, 30, 10]);

    translate([-1, 7, 4])
        rotate([0,90,0])
            cylinder(h=4, d=3);
            
    translate([-1, 30-7, 4])
        rotate([0,90,0])
            cylinder(h=4, d=3);
}

translate([2, 30, 10])
    rotate([-90,0,-90])
        fillet(3, 30);

translate([0,0,10])
{
    cube([width, 30, 3]);
    translate([0,30-3,3])
    {
        translate([width,0,0])
            rotate([0,0,180])
                fillet(3, width);
        cube([width, 3, 5]);
        translate([0,1.5,5])
            rotate([0,90,0])
                cylinder(h=width, d=3);
    }
}

module fillet(radius, length)
{
    difference()
    {
        cube([length, radius, radius]);
        translate([0,radius,radius])
        {
            rotate([0,90,0])
            {
                cylinder(h=length, d=radius*2);
            }
        }
    }
}