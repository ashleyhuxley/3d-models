$fn=30;

bracket();

module bracket()
{
    difference()
    {
        cube([10, 12, 80]);
        
        for (i = [0 : 5])
        {
            translate([-0.1, 4, 8 + (i*12)])
            {
                rotate([0, 90, 0])
                {
                    cylinder(h = 10.2, d = 5);
                }
                
                rotate([45,0,0])
                {
                    translate([0,-2.5,0])
                        cube([10.2, 5, 15]);
                }
                
                translate([5,0,-0])
                {
                    rotate([160,0,0])
                    {
                        cylinder(h=9, d=4);
                        translate([-2, 0, 0])
                        {
                            cube([4, 4.1, 9]);
                        }
                    }
                }
            }
        }
        
        translate([5, 15, 14.5])
        {
            rotate([90,0,0])
            {
                cylinder(h=10, d=3.8);
            }
        }
        translate([5, 15, 75])
        {
            rotate([90,0,0])
            {
                cylinder(h=10, d=3.8);
            }
        }
    }
}

module hook()
{
    translate([30,0,0])
    {
        cylinder(h = 130, d=3);
        translate([0,0,130])
        {
            translate([-2.5, 0, 0])
            {
                rotate([0,90,0])
                {
                    cylinder(h=5, d=4.5);
                }
                sphere(2.25);
            }
            translate([2.5,0,0])
            {
                sphere(2.25);
            }
        }
        
        translate([1.5,0,0])
        {
            rotate([0,-90,0])
            {
                difference()
                {
                    cylinder(h=6.5, d=6);
                    cylinder(h=6.5, d=3);
                }
            }
        }
    }
}