include <modules.scad>

width = 4;
id = 4;
wall = 0.8;
wing = 3;
cr = 1.5;

od = id + (wall * 2);

$fn=20;

difference()
{
    union()
    {
        rounded_rect(width, od + (wing * 2), wall, cr);

        translate([-width/2, 0, id/2])
        {
            rotate([0,90,0])
            {
                cylinder(h = width, d = od);
            }
            translate([0, -(od/2), -(od/2)])
            {
                cube([width, od, (od/2)]);
            }
        }
    }
    
    translate([-width/2, 0, id/2])
    {
        rotate([0,90,0])
        {
            cylinder(h = width, d = id);
        }
        translate([0, -(id/2), -(id/2)])
        {
            cube([width, id, id/2]);
        }
    }
    
    translate([-width/2, -(od/2), -wall])
    {
        cube([width, od, wall]);
    }
}