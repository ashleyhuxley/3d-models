difference()
{
	cylinder(h=8, d=110, $fn=120);
	translate([0,0,2])
		cylinder(h=8, d=106, $fn=120);

	translate([0,0,-0.1])
	{
		cylinder(h=3, d=20, $fn=30);

		rotate([0,0,0]) translate([35,0,0]) cylinder(h=3, d=20, $fn=30);
		rotate([0,0,60]) translate([35,0,0]) cylinder(h=3, d=20, $fn=30);
		rotate([0,0,120]) translate([35,0,0]) cylinder(h=3, d=20, $fn=30);
		rotate([0,0,180]) translate([35,0,0]) cylinder(h=3, d=20, $fn=30);
		rotate([0,0,240]) translate([35,0,0]) cylinder(h=3, d=20, $fn=30);
		rotate([0,0,300]) translate([35,0,0]) cylinder(h=3, d=20, $fn=30);
	}
}