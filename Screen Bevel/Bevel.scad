iw = 52;
il = 72;
bw = 3;
bh = 2;
ol = 1;

difference()
{
	polyhedron(
		points = [
			[0,0,0],
			[bw + il + bw, 0,0],
			[bw + il + bw, bw + iw + bw,0],
			[0, bw + iw + bw,0],
			[bw, bw, bh],
			[bw + il, bw, bh],
			[bw + il, bw + iw, bh],
			[bw, bw + iw, bh]
		],
		faces = [
			[0,1,2,3],
			[0,4,5,1],
			[1,5,6,2],
			[2,6,7,3],
			[3,7,4,0],
			[4,7,6,5]
		]
	);

	translate([bw + ol, bw + ol, -0.1])
		cube([il - (ol * 2), iw - (ol * 2), bh + 0.2]);
}

