include <modules.scad>;

$fn=120;

badge();

module badge()
{
    difference()
    {
        rounded_cylinder(4, 41.5, 0.5);

        translate([0, 38, -0.1])
        {
            cylinder(h=1, d1=5, d2=3);
            cylinder(h=4.2, d=4);
            translate([0,0,3.1])
                cylinder(h=1.1, d1=3, d2=5);
        }
    }
}

module rounded_cylinder(height, radius, corner_radius)
{
    union()
    {
        cylinder(h=height, r=radius-(corner_radius));
        
        translate([0,0,corner_radius])
            cylinder(h=height-(corner_radius*2), r=radius);
        
        translate([0,0,corner_radius])
            torus(corner_radius, radius-corner_radius);
        
        translate([0,0,height-corner_radius])
            torus(corner_radius, radius-corner_radius);
    }
}

module torus(r1, r2)
{
    rotate_extrude(convexity = 10)
        translate([r2, 0, 0])
            circle(r = r1, $fn=16);
}