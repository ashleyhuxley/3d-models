difference()
{
    union()
    {
        cube([20, 40, 2]);
        translate([10,5,0])
            cylinder(h = 8, d = 4, $fn=30);
    }
    
    translate([5, 6.2, 2])
        cube([8, 1, 4]);
}