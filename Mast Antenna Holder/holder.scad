include <threads.scad>;

$fn = 120;

id = 25.8;

difference()
{
    union()
    {
        cylinder(d=30, h=15);
        translate([id/2,0,7.5])
            rotate([0,90,0])
                cylinder(d=10, h=5);
    }
    
    cylinder(d=id, h=15);
    translate([(id/2)-3,0,7.5])
        rotate([0,90,0])
            metric_thread(8, 1.5, 8, internal=true, leadin=1);
}