difference()
{
    cylinder(h = 40, d1 = 32, d2 = 32, $fn = 50);
    cylinder(h = 40, d1 = 28, d2 = 28, $fn = 50);
}