innerX = 50;
innerY = 33.5;
wall = 3;
depth = 18.5 + wall;

powerHole = 10.5 / 2;

outerX = innerX + (wall * 2);
outerY = innerY + (wall * 2);

drill = 1.5;
drillDepth = 5;
outerDrill = drill + 3;

f = 20;

difference() {
    union() {
        difference() {
            cube([outerX,outerY,depth]);
            translate([wall, wall, wall]) {
                cube([innerX, innerY, depth - wall]);
            }
        }
        
        translate([outerDrill, outerDrill, 0])
        {
            cylinder(depth, outerDrill, outerDrill, $fn=f);
        }
        translate([outerX - outerDrill, outerDrill, 0])
        {
            cylinder(depth, outerDrill, outerDrill, $fn=f);
        }
        translate([outerDrill, outerY - outerDrill, 0])
        {
            cylinder(depth, outerDrill, outerDrill, $fn=f);
        }
        translate([outerX - outerDrill, outerY - outerDrill, 0])
        {
            cylinder(depth, outerDrill, outerDrill, $fn=f);
        }
        
        // Circuit Slots
        translate([22, wall, 0]) {
            cube([2, 2, depth]);
        }
        translate([26, wall, 0]) {
            cube([2, 2, depth]);
        }
        translate([22, innerY + wall - 2, 0]) {
            cube([2, 2, depth]);
        }
        translate([26, innerY + wall - 2, 0]) {
            cube([2, 2, depth]);
        }
    }
    
    // Case Screw Holes
    translate([outerDrill, outerDrill, depth - drillDepth]) {
        cylinder(drillDepth, drill, drill, $fn=f);
    }
    translate([outerX - outerDrill, outerDrill, depth - drillDepth]) {
        cylinder(drillDepth, drill, drill, $fn=f);
    }
    translate([outerDrill, outerY - outerDrill, depth - drillDepth]) {
        cylinder(drillDepth, drill, drill, $fn=f);
    }
    translate([outerX - outerDrill, outerY - outerDrill, depth - drillDepth]) {
        cylinder(drillDepth, drill, drill, $fn=f);
    }
    
    // Power Socket
    translate([outerDrill * 2 + wall * 2,wall + 0.1,((depth - wall) / 2) + wall ]) {
        rotate([90, 0, 0]) {
            cylinder(wall + 0.2, powerHole, powerHole);
        }
    }
    
    // FTDI Header
    translate([28, outerY - wall, wall]) {
        cube([4.5, wall, depth - wall]);
    }
}


