innerX = 45;
innerY = 40;
wall = 3;
depth = 5;

outerX = innerX + (wall * 2);
outerY = innerY + (wall * 2);

drill = 1.5;
outerDrill = 2 + wall;

f = 20;

difference() {
    union() {
        difference() {
            translate([outerX / 2, outerY / 2, 0]) {
                roundedRect([outerX-outerDrill,outerY-outerDrill,depth], outerDrill);
            }
            translate([wall, wall, 1]) {
                cube([innerX, innerY, depth - 1]);
            }
        }
        
        translate([outerDrill, outerDrill, 0])
        {
            cylinder(depth, outerDrill, outerDrill, $fn=f);
        }
        translate([outerX - outerDrill, outerDrill, 0])
        {
            cylinder(depth, outerDrill, outerDrill, $fn=f);
        }
        translate([outerDrill, outerY - outerDrill, 0])
        {
            cylinder(depth, outerDrill, outerDrill, $fn=f);
        }
        translate([outerX - outerDrill, outerY - outerDrill, 0])
        {
            cylinder(depth, outerDrill, outerDrill, $fn=f);
        }
    }
    
    translate([outerDrill, outerDrill, depth - drillDepth]) {
        cylinder(depth, drill, drill, $fn=f);
        cylinder(2.5, 2.75, 2.75, $fn=f);
    }
    translate([outerX - outerDrill, outerDrill, depth - drillDepth]) {
        cylinder(depth, drill, drill, $fn=f);
        cylinder(2.5, 2.75, 2.75, $fn=f);
    }
    translate([outerDrill, outerY - outerDrill, depth - drillDepth]) {
        cylinder(depth, drill, drill, $fn=f);
        cylinder(2.5, 2.75, 2.75, $fn=f);
    }
    translate([outerX - outerDrill, outerY - outerDrill, depth - drillDepth]) {
        cylinder(depth, drill, drill, $fn=f);
        cylinder(2.5, 2.75, 2.75, $fn=f);
    }
}

module roundedRect(size, radius)
{
    x = size[0];
    y = size[1];
    z = size[2];

    linear_extrude(height=z)
    hull()
    {
        // place 4 circles in the corners, with the given radius
        translate([(-x/2)+(radius/2), (-y/2)+(radius/2), 0])
        circle(r=radius);

        translate([(x/2)-(radius/2), (-y/2)+(radius/2), 0])
        circle(r=radius);

        translate([(-x/2)+(radius/2), (y/2)-(radius/2), 0])
        circle(r=radius);

        translate([(x/2)-(radius/2), (y/2)-(radius/2), 0])
        circle(r=radius);
    }
}
