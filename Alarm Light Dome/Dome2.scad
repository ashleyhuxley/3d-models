include <modules.scad>
include <threads.scad>

$fn = 120;

//base();

//translate([100, 0, 0]) 
  //rotate([180,0,0]) 
    cover();

module base()
{
    difference()
    {
        union()
        {
            rounded_rect(75, 75, 1.5, 10);

            translate([0,0,1.5])
            {
                metric_thread(64, 1.5, 10, leadin=1);
            }
        }
        
        cylinder(d=35, h=20);
        
        translate([0,0,0.6])
        {
            cylinder(d=60, h=20);
        }
    }
}

module cover()
{
    difference()
    {
        cylinder(d=70, h=10);
        translate([0,0,-0.1])
            metric_thread(65, 1.5, 10.2, leadin=3, internal=1);
    }
    
    translate([0,0,10])
    {
        dome(70, 1);
    }
}

module dome(diameter, thickness)
{
    radius = diameter / 2;
    difference()
    {
        sphere(d=diameter);
        sphere(d=diameter - thickness);
        translate([-radius,-radius,-radius])
        {
            cube([diameter, diameter, radius]);
        }
    }
}