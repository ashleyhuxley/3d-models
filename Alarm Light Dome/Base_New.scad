include <modules.scad>

$fn=120;


difference()
{
    union()
    {
        rounded_rect(75, 75, 1.5, 10);
        cylinder(d=70, h=10);
        translate([0,0,5])
        {
            rotate([0,90,0])
            {
                translate([0,0,-36])
                    cylinder(d=4, h=72);
            }
        }
    }
    
    translate([0,0,-1])
        cylinder(d=65, h=20);
}