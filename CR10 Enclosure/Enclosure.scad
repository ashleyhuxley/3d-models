// Internal Size: 50 x 70 x 62
b1_width = 480;
b2_width = 620;

ylength = 700-(28*2);
zlength = 620-(18*2);

echo(ylength);
echo(zlength);


box();

module box()
{
    base();
    translate([0,0,zlength+18])
        base();
        
    // Back
    translate([10,700,0])
        cube([b1_width,2,620]);

    // Side
    translate([-2,0,0])
        cube([2, 700, b2_width]);
        
    // Top
    translate([10,0,620])
        cube([b1_width, 700, 2]);

    translate([0,0,18])
    {
        translate([0,0,0])
            zbaton(zlength);
        translate([500-18,0,0])
            zbaton(zlength);
        translate([0,700-28,0])
            zbaton(zlength);
        translate([500-18,700-28,0])
            zbaton(zlength);
    }
}

module base()
{
    translate([0,0,0])
        xbaton(500);
    translate([0,ylength+28,0])
        xbaton(500);

    translate([0,28,0])
        ybaton(ylength);
    translate([500-28, 28, 0])
        ybaton(ylength);
}

module zbaton(l)
{
    color([0.3,0.3,1])
        cube([18, 28, l]);
}

module ybaton(l)
{
    color([0,1,0])
        cube([28,l,18]);
}

module xbaton(l)
{
    color([1,0,0])
        cube([l, 28, 18]);
}