include <threads.scad>

$fn=50;

nut();

module insert()
{
	difference()
	{
		union()
		{
			translate([0,0,8])
				cylinder(h = 33, d=44);
			translate([0,0,40])
				cylinder(h=1, d=50);

			metric_thread(44, 1.5, 8, leadin=3);
		}

		cylinder(h = 41, d=41);
	}
}

module nut()
{
	difference()
	{
		cylinder(h=5, d=55, $fn=8);
		metric_thread(45, 1.5, 5, leadin=2, internal=1);
	}
}