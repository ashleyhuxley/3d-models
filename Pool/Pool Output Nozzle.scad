outer_diameter=26.5;
barb1 = 1.5;
l1 = 50;
thickness = 2;
resolution=60;
barb_offset = (outer_diameter / 2) + 13;
barb1_length = 5;
barb1_spacing = 8;
barb1_count = 3;

coulpling_length=0.1;
taper_displacement=.1;
$fn = (resolution == 4) ?  4 : max(2 * floor(resolution / 2), 8);
inner_diameter = outer_diameter - 2 * thickness;
ep = 0.05;
corner_lineup = 360 / $fn / 2;

elbow();

module elbow()
{
    difference()
    {
        outer_elbow();
        inner_elbow();
    }
}

module outer_elbow()
{
    rotate([90, -corner_lineup, 0])
    {
        cylinder(h = l1, d = outer_diameter);
        translate([0, 0, barb_offset])
        {
            for (b1z = [0 : 1 : barb1_count - 1])
            {
                translate([0,0, barb1_spacing * b1z])
                {
                    cylinder(d1 = outer_diameter + barb1 * 2, d2 = outer_diameter, h = barb1_length);
                }
            }
        }
    }
    
    sphere(d = outer_diameter);
    
    rotate([0, 90, 0])
    {
        rotate([0, 0, corner_lineup])
        {
            cylinder(h = (.6 + coulpling_length) * outer_diameter, d = outer_diameter);
        }
    }
}

module inner_elbow()
{
    rotate([90, corner_lineup, 0])
    { 
        cylinder(h = l1, d = inner_diameter);
    }
    
    sphere(d = inner_diameter);
    
    rotate([0, 90, 0])
    {
        rotate([0, 0, corner_lineup])
        {
            cylinder(h = outer_diameter + ep, d = inner_diameter);
        }
    }
}