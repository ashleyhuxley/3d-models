include <threads.scad>

// Barbed part
od=26.5;
barb = 1.2;
thick = 2;
barb_length = 5;
barb_spacing = 8;
barb_count = 3;
id = od - 2 * thick;


//////////////////////// Parameters ////////////////////////////////
//is the diameter (in mm) of the corner and the untapered diameter. If fitting OVER your pipe, set this to (pipe diameter + 2*thickness+0.5)*1.03.
outer_diameter=od;
//(in mm) of a single wall of the coupler
thickness=2;
//is the number of segments that make up a circle.
resolution=60;
//is the diameter (in mm) of the corner of the tapered diameter. If fitting OVER your pipe, set this to (pipe diameter + 2*thickness+0.5)*1.03.
output_diameter=33.5;
// times the outer diameter is the length that the pipes will couple by
coulpling_length=0.1;
//times the outer diameter is the length that the outer diameter tapers to the output diameter over
tapering_length=0.4;
//times the outer diameter is the distance between the corner and the taper. Range=[0,∞)
taper_displacement=.1;
///////////////// Derived Parameters //////////////////////////
//4 works, anything else lower than 8 becomes 8, everything else is rounded to the next lower even number. Evens are needed because spheres cannot have an odd number of facets, which leads to cylinder/sphere mismatches which drastically increases number of rendered facets.
$fn=(resolution==4)? 4: max(2*floor(resolution/2),8);
inner_diameter=outer_diameter-2*thickness;
//a small value used for elimitating edge artifacts
ep=0.05+0;
//so the edges line up
corner_lineup=360/$fn/2;
//////////////////// Main() ////////////////////////////////////
cone3();
elbow();
rotate([0,90,0])
{
	translate([0,0,18.5])
		rotate([0,0,corner_lineup]) plug();
}

translate([0,-50,0])
	rotate([90,0,0])
		tablet();

translate([0,-80,0])
	rotate([90,0,0])
		cap();

translate([0,-29,0])
	rotate([90,3,0])
		thread();



////////////////// Functions ///////////////////////////////////

module thread()
{
	difference()
	{
		metric_thread(37, 3, 10, internal=0, leadin=1);
		cylinder(h=10, d=output_diameter-2*thickness);
	}
}

module cap()
{
	difference()
	{
		cylinder(h=31, d=40);
		translate([0,0,-0.05]) metric_thread(38, 3, 10, internal=1, leadin=2);
		translate([0,0,9.95]) cylinder(h=19, d=35);

		for(r = [0 : 20 : 180])
			rotate([0,0,r])
				for (i = [14 : 6 : 26])
					translate([0,21,i]) rotate([90,0,0])
						cylinder(h=42, d=5);

		translate([0,0,28]) cylinder(h = 4, d = 5);
		for(r = [0 : 45 : 360]) rotate([0,0,r]) translate([8,0,28]) cylinder(h = 4, d = 5);
		for(r = [0 : 30 : 360]) rotate([0,0,r]) translate([14,0,28]) cylinder(h = 4, d = 5);
	}
}

module tablet()
{
	cylinder(h = 17.5, d=30.5, $fn=30);
}

module cone3(){
    translate([0,-(.5+taper_displacement)*outer_diameter+ep,0]) 
    rotate([90,corner_lineup,0]){
        taper();
        output();
        }
    }

module taper(){
    difference(){
        cylinder(d1=outer_diameter,h=tapering_length*outer_diameter,d2=output_diameter);
        translate([0,0,-ep]) cylinder(d1=inner_diameter,h=tapering_length*outer_diameter+2*ep,d2=output_diameter-2*thickness);
        }
    }
    
module output(){
    translate([0,0,(tapering_length)*outer_diameter-ep])
    difference(){
        cylinder(d=output_diameter, h=(coulpling_length)*outer_diameter);
        translate([0,0,-ep])
        cylinder(d=output_diameter-2*thickness, h=(1.1*coulpling_length)*outer_diameter+ep);
        }
    }
    

module elbow(){
    difference(){
        outer_elbow();
        inner_elbow();
        }
    }
module outer_elbow(){
    rotate([90,-corner_lineup,0]) cylinder(h=(.5+taper_displacement)*outer_diameter, d=outer_diameter);
    sphere(d=outer_diameter);
    rotate([0,90,0]) rotate([0,0,corner_lineup]) cylinder(h=(.6+coulpling_length)*outer_diameter, d=outer_diameter);
    }
module inner_elbow(){
    rotate([90,corner_lineup,0]) cylinder(h=outer_diameter+ep, d=inner_diameter);
    sphere(d=inner_diameter);
    rotate([0,90,0]) rotate([0,0,corner_lineup]) cylinder(h=outer_diameter+ep, d=inner_diameter);
    }

module plug()
{
	difference()
	{
		union()
		{
			cylinder(h = (barb_spacing * barb_count), d = od);
			translate([0, 0, (barb_spacing - barb_length)])
			{
				for (b1z = [0 : 1 : barb_count - 1])
				{
					translate([0,0, barb_spacing * b1z])
					{
						cylinder(d1 = outer_diameter + barb * 2, d2 = od, h = barb_length);
					}
				}
			}
		}
    
		cylinder(h = (barb_spacing * barb_count), d = id);
	}
}

