include <threads.scad>
$fn=30;

length = 50;

difference()
{

    union()
    {
        thread();
        translate([0,0,10])
            body();
        translate([0,0,10+length])
            nozzle();
    }

    translate([-50,0,0,])
    {
        //cube([100,100,100]);
    }

}

module thread()
{
    difference()
    {
        cylinder(h=10, d=40);
        translate([0,0,-0.05]) metric_thread(38, 3, 10, internal=1, leadin=2);
    }  
}

module body()
{
    difference()
    {
        cylinder(h=length, d=40);
        
        translate([0,0,-0.1])
        {
            cylinder(h=length+0.2, d=4);
            
            tubes(7, 5);
            tubes(14, 10);
            tubes(21, 15);           
        }
        
        translate([0,0,30])
            cylinder(h=20.1, d1=0, d2=37);

    }
}


module nozzle()
{
    difference()
    {
        cylinder(15, d=40);
        cylinder(15, d=37);
    }
    
    translate([0,0,15])
    {
        difference()
        {
            cylinder(h=26, d1=40, d2=18);
            cylinder(h=26, d1=37, d2=15);
        }
    }
    
    translate([0,0,41])
    {
        difference()
        {
            cylinder(5, d=18);
            cylinder(5, d=15);
        }
    }
}

module tubes(count, offset)
{
    for (i = [0 : count - 1])
    {
        rotate([0,0,((360/count)*i)])
            translate([offset,0,0])
                cylinder(h=length+0.2, d=4);
    }
}
