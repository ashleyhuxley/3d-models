include <threads.scad>

cap();

module cap()
{
	difference()
	{
		cylinder(h=15, d=40);
		translate([0,0,-0.05]) metric_thread(38, 3, 10, internal=1, leadin=2);
		translate([0,0,9.95]) cylinder(h=(15-9.95)+0.1, d=35);
	}

	translate([0,0,15])
	{
		difference()
		{
			cylinder(h=22, d1=40, d2=26);
			cylinder(h=22, d1=37, d2=23);
		}
	}
}