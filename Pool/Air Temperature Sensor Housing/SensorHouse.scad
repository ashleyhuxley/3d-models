fn = 60;



translate([0,0,0]) dom(8, 35, 25, 2.5);
translate([0,0,8]) dom(8, 35, 25, 2.5);
translate([0,0,16]) dom(8, 35, 25, 2.5);
translate([0,0,24]) cone(12, 35, 2.5);

translate([0,0,-3])
{
	//cylinder(h=25, d=5, $fn=fn);

	difference()
	{
		cylinder(h=3, d=9, $fn=fn);
		translate([0,0,-0.1]) cylinder(h=3.2, d=5, $fn=fn);
	}

	rotate([0,0,90]) translate([-3,3,0]) cube([6, 14, 3]);
	rotate([0,0,270]) translate([-3,3,0]) cube([6, 14, 3]);

	difference()
	{
		translate([-18,-6,-10]) cube([2,12,37]);
		rotate([0,90,0])
		{
			translate([5,0,-18]) cylinder(h=2, d1=3.5, d2=6, $fn=fn);
		}
	}
}


translate([-16,0,0]) cylinder(h=24, d=2.5, $fn=20);
translate([16,0,0]) cylinder(h=24, d=2.5, $fn=20);
translate([0,-16,0]) cylinder(h=24, d=2.5, $fn=20);
translate([0,16,0]) cylinder(h=24, d=2.5, $fn=20);

module dom(height, dia1, dia2, iw)
{
	difference()
	{
		cylinder(h = height, d1 = dia1, d2= dia2, $fn=fn);
		translate([0,0,-0.1]) cylinder(h = height + 0.2, d1 = dia1 - (iw * 2), d2= dia2 - (iw * 2), $fn=fn);
	}
}


module cone(height, dia1, iw)
{
	difference()
	{
		cylinder(h = height, d1 = dia1, d2= 0, $fn=fn);
		translate([0,0,-0.1]) cylinder(h = height + 0.2 - iw, d1 = dia1 - (iw * 2), d2= 0, $fn=fn);
	}
}