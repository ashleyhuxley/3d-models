outer_diameter=26.5;
barb = 1.2;
thickness = 2;
barb_length = 5;
barb_spacing = 8;
barb_count = 3;
$fn = 60;
inner_diameter = outer_diameter - 2 * thickness;


plug();

translate([0,0,-45])
	socket();

translate([0,0,-10])
	joint();


module joint()
{
	difference()
	{
		cylinder(h = 10, d1 = 40, d2 = outer_diameter);
		cylinder(h = 10, d1 = 33, d2 = inner_diameter);
	}
}

module plug()
{
	difference()
	{
		union()
		{
			cylinder(h = (barb_spacing * barb_count), d = outer_diameter);
			translate([0, 0, (barb_spacing - barb_length)])
			{
				for (b1z = [0 : 1 : barb_count - 1])
				{
					translate([0,0, barb_spacing * b1z])
					{
						cylinder(d1 = outer_diameter + barb * 2, d2 = outer_diameter, h = barb_length);
					}
				}
			}
		}
    
		cylinder(h = (barb_spacing * barb_count), d = inner_diameter);
	}
}

module socket()
{
	difference()
	{
		union()
		{
			cylinder(h = 35, d1 = 38.5, d2=40);
			translate([0,0,18]) torus(39/2, 0.7);
		}
		cylinder(h = 35, d = 33);
	}
}

module torus(full_radius, profile_radius)
{
	rotate_extrude(convexity = 10)
		translate([full_radius, 0, 0])
			circle(r = profile_radius);
}