fn=120;

difference()
{
	union()
	{
		cylinder(h=14, d=47, $fn=fn);
		cylinder(h=3, d=55.5, $fn=fn);
	}

	cylinder(h=14, d=38, $fn=fn);
}