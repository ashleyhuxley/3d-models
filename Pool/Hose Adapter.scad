// Bottom End
od1 = 32;
wall1 = 2;
barb1 = 1;
l1 = 20;

barb1_length = l1 / 5;
barb1_spacing = l1 / 3.5;
barb1_count = 3;

// Top End
od2 = 22;
wall2 = 2;
barb2 = 0.5;
l2 = 20;

barb2_length = l2 / 5;
barb2_spacing = l2 / 3.5;
barb2_count = 3;

// Middle
lm = 10;

fn=30;

id1 = od1 - (wall1 * 2);
id2 = od2 - (wall2 * 2);

difference()
{
    union()
    {
        // Bottom section
        cylinder(d = od1, h = l1, $fn=fn);
        for (b1z = [0 : 1 : barb1_count - 1])
        {
            translate([0,0, barb1_spacing * b1z])
            {
                cylinder(d1 = od1, d2 = od1 + barb1 * 2, h = barb1_length, $fn=fn);
            }
        }

        // Mid section
        translate([0,0,l1]) cylinder(d1 = od1, d2 = od2, h = lm, $fn = fn);

        // Top section
        translate([0,0,l1 + lm])
        {
            cylinder(d1 = od2, d2 = od2, h = l2, $fn = fn);
            for (b2z = [barb2_count - 1 : -1 : 0])
            {
                translate([0, 0, l2 - (barb2_spacing * b2z) - barb2_length])
                {
                    cylinder(d1 = od2 + barb2 * 2, d2 = od2, h = barb2_length, $fn=fn);
                }
            }
        }
    }

    // Bottom cutout
    cylinder(d = od1 - (wall1 * 2), h = l1, $fn=fn);
    
    // Middle cutout
    translate([0,0,l1]) cylinder(d1 = od1 - (wall1 * 2), d2 = od2 - (wall2 * 2), h = lm, $fn=fn);
    
    // Top cutout
    translate([0,0,l1 + lm]) cylinder(d = od2 - (wall2 * 2), h = l2, $fn=fn);
}