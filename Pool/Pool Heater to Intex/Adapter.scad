include <threads.scad>
include <threads2.scad>

intex_side();

translate([0,0,23])
{
	mid_section();
}

translate([0,0,25])
{
	heater_side();
}




module intex_side()
{
	difference()
	{
		metric_thread(58.8, 2.5, 23, leadin=3, test=false);
		cylinder(h=23, d=47, $fn=50);
	}
}

module mid_section()
{
	difference()
	{
		cylinder(h=2, d1=58.8, d2=58, $fn=50);
		cylinder(h=2, d1=47, d2=42, $fn=50);
	}
}

module heater_side()
{
	difference()
	{
		union()
		{
			translate([0,0,11]) triangle_thread(62, 3, 14,leadin=1);
			cylinder(h=30, d=58, $fn=50);
		}

		cylinder(h=30, d=42, $fn=50);
		translate([0,0,30-2.6])
			donut(2.61, 54.5, 47.6, 50);
	}
}

module donut(height, dia1, dia2, fn)
{
	difference()
	{
		cylinder(h=height, d=dia1, $fn=fn);
		cylinder(h=height, d=dia2, $fn=fn);
	}
}