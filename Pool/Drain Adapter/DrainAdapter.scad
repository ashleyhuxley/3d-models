include <threads.scad>;

outer();

//translate([0,0,10]) outer();

module inner()
{
    difference()
    {
        union()
        {
            cylinder(h=35, d=31);
            translate([0,0,16])
            {
                cylinder(h=3, d=38);
            }
            
            translate([0,0,13])
            {
                cylinder(h=3, d1=31, d2=38);
            }
        }
        
        translate([0,0,-0.1])
        {
            cylinder(h=40.2, d=26);
        }
        
        translate([0,0,7])
        {
            torus(2, 16);
        }
    }
}

module outer()
{
    difference()
    {
        ScrewHole(43, 13.5, pitch=2)
        {
            cylinder(h=15, d=50);
            p = 12;
            for (i = [0 : p])
            {
                rotate([0, 0, i*(360/p)])
                {
                    translate([24,0,-0])
                    {
                        cylinder(h=15, d=5, $fn=30);
                    }
                }
            }
        }
        
        cylinder(h=16, d=33);
    }
}

module torus(r1, r2)
{
    rotate_extrude(convexity = 10, $fn = 100)
        translate([r2, 0, 0])
            circle(r = r1, $fn = 30);
}