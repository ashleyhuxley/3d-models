include <threads.scad>
include <modules.scad>

$fn = 120;

bleed_screw();

module lid()
{
    difference()
    {
        ScrewHole(16, 20, pitch=3)
        {
            ScrewHole(116.5, 15, pitch=4)
            {
                cylinder(h = 15, d = 124);
            }

            translate([0,0,10])
            {
                cylinder(h = 5, d = 105);
            }
            
            translate([0,0,15])
                torus(2, (121/2)-0.5);
            
            translate([0,0,15])
                cylinder(h = 2, d=119.5);
        }
        
        c = 30;
        for (i = [0: c])
        {
            rotate([0, 0, i*(360/c)])
            {
                translate([63.9,0,-0.1])
                {
                    cylinder(h=20, d=5);
                }
            }
        }
        
        translate([0,0,15])
        {
            cylinder(h=2.1, d1=16, d2=20);
        }
    }
}

module torus(r1, r2)
{
    rotate_extrude(convexity = 10, $fn = 100)
        translate([r2, 0, 0])
            circle(r = r1, $fn = 100);
}

module bleed_screw()
{
    difference()
    {
        cylinder(h=10, d=30);
        c = 15;
        for (i = [0: c])
        {
            rotate([0, 0, i*(360/c)])
            {
                translate([16.5,0,-0.1])
                {
                    cylinder(h=10.2, d=5);
                }
            }
        }
    }
    
    translate([0,0,10])
    {
        cylinder(h=2, d=23);
    }
    
    translate([0,0,12])
    {
        ScrewThread(16, 13, 3);
    }
}