include <metric_threads.scad>
include <modules.scad>

$fn=60;

thread_height = 15;
offset_height = 2;
pipe_height = 32;

full_height = thread_height + offset_height + pipe_height;

//internal(58);    //58

//translate([0,60,0])
mirror_copy([0,0,1])
{
    external(90);
}
translate([0,0,45])
{
    difference()
    {
        tube(80, 67, 60);
        translate([-35,-35,-1])
        {
            rotate([0,-45,0])
            {
                cube([140,70,70]);
            }
        }
    }
}

module foo()
{
    difference()
    {
        cylinder(h=full_height, d=64);
        
        metric_thread(58.8, 2.5, thread_height, leadin=3, test=false, internal=true);
        
        translate([0, 0, -0.1])
            cylinder(h=full_height + 0.2, d=32);
        
        translate([0, 0, thread_height])
            cylinder(h=offset_height, d=56.2);
        
        translate([0, 0, thread_height + offset_height])
            cylinder(h=8, d1=56.2, d2=32);
    }
}

module internal(height)
{
    tube(height, 46, 40);
}

module external(height)
{
    difference()
    {
        cylinder(h=height/2, d=67);
        cylinder(d=60, h=(height/2)-20);

        translate([0,0,(height/2)-20])
        {
            metric_thread(60, 2.5, 20, leadin=1, test=false, internal=true);
            translate([10.5,0,0])
            {
                //cylinder(d=40, h=20);
            }
            translate([-10.5,0,0])
            {
                //cylinder(d=40, h=20);
            }
        }
    }
}