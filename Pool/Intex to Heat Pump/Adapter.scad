include <metric_threads.scad>
include <modules.scad>

$fn = 60;


difference()
{
    union()
    {
        metric_thread(31, 2, 15, leadin=3, test=false, internal=false);

        translate([0,0,15])
            cylinder(h=10, d=44, $fn=6);
            
        translate([0,0,15 + 10])
        {
            metric_thread(58.8, 2.5, 21, leadin=1, test=false, internal=false);
            cylinder(h=25, d=55);
        }
    }
    
    translate([0,0,-1])
        cylinder(d=24, h=47);
    
    translate([0,0,25])
        cylinder(d1=24, d2=40, h=25.1);
    
    translate([0,0,48.8])
    rotate_extrude(convexity = 10)
    translate([25, 0, 0])
        square(size = [3.2, 3.2], center=true);
}


