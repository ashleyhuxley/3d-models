// 2.5
// 81
// 32

include <threads.scad>

ScrewHole(81, 12, pitch=2.5)
{
    cylinder(h=12, d=85);
}