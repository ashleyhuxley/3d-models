$fn = 30;

radius = 3;

width = 30;
length = 80;
height = 12;

dia = radius * 2;

translate([radius,radius,0])
{
    hull()
    {
        translate([0,0,0])
            post();
        translate([width-dia,0,0])
            post();
        translate([0,length-dia,0])
            post();
        translate([width-dia,length-dia,0])
            post();
    }
}

module post()
{
    cylinder(h = height-radius, r=radius);
    translate([0,0,height-radius])
        sphere(radius);
}