//cover(32, 15.5, 25, 27);
cover(30, 20, 22, 21.2);

module cover(bottom_width, pipe_diameter, height, wall_distance)
{
	top_width = pipe_diameter + 3;
	wall_distance_c = wall_distance - (pipe_diameter / 2);

	inset = (bottom_width-top_width) / 2;

	difference()
	{
		union()
		{
			cylinder(h = height, d1= bottom_width, d2 = top_width);
			translate([-(bottom_width/2),0,0])
				polyhedron(
					points=[
						[0,0,0],
						[bottom_width,0,0],
						[bottom_width,wall_distance_c,0],
						[0,wall_distance_c,0],
						[inset,0,height],
						[bottom_width - inset,0,height],
						[bottom_width - inset,wall_distance_c,height],
						[inset,wall_distance_c,height]
					],
					faces =[
						[0,1,2,3],
						[4,5,1,0],
						[5,6,2,1],
						[6,7,3,2],
						[7,4,0,3],
						[7,6,5,4]
					]);
		}

		cylinder(h = height, d = pipe_diameter);
		translate([-(pipe_diameter/2),0,0])
		{
			cube([pipe_diameter, wall_distance_c + 20, height]);
		}
	}
}