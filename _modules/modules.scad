module pie_slice(r=5.0, a=45)
{
  polygon(points=[
    [0, 0],
    for(theta = 0; theta < a; theta = theta + $fa)
      [r * cos(theta), r * sin(theta)],
    [r * cos(a), r * sin(a)]
  ]);
}

module hexagon(w, h)
{
    translate([0,0,h/2])
    {
        hull()
        {
            cube([w / 1.7, w, h], center = true);
            rotate([0,0,120])
            {
                cube([w / 1.7, w, h], center = true);
            }
        }
    }
}

module fillet(r, h) {
    translate([r / 2, r / 2, 0])

        difference() {
            cube([r + 0.01, r + 0.01, h], center = true);

            translate([r/2, r/2, 0])
                cylinder(r = r, h = h + 1, center = true);

        }
}

module space_out(count, spacing)
{
    for (i = [0 : 1 : count - 1])
    {
        translate([i * spacing, 0, 0])
        {
            children();
        }
    }
}

module countersink(depth, cd1, cd2)
{
    cylinder(h = depth, d1 = cd1, d2 = cd2);
}

module countersunk_hole(hole_dia, hole_length, cs_depth, cs_dia1)
{
	cylinder(h = cs_depth, d1 = cs_dia1, d2 = hole_dia);
	translate([0,0,cs_depth])
		cylinder(h=hole_length, d=hole_dia);
}

module mirror_copy(v = [1, 0, 0])
{
    children();
    mirror(v) children();
}

module stadium(dia, height, width)
{
    hull()
    {
        translate([(width/2),0,0]) cylinder(h = height, d = dia);
        translate([-(width/2),0,0]) cylinder(h = height, d = dia);
    }
}

module quads(width, height)
{
    h2 = height / 2;
    w2 = width / 2;
    
    translate([-h2, -w2, 0]) children();
    translate([-h2, w2, 0]) children();
    translate([h2, -w2, 0]) children();
    translate([h2, w2, 0]) children();
}

module rounded_rect(length, width, height, radius)
{
	hull()
	{
		quads(length - radius - radius, width - radius - radius)
		{
			cylinder(h = height, r = radius);
		}
	}
}

module box(length, width, height, wall)
{
	difference()
	{
		cube([length, width, height]);
		translate([wall,wall,wall])
			cube([length-(wall*2), width-(wall*2), height-wall+0.1]);
	}
}

module clip(width, angle, inner_diameter, thickness, length)
{
	inner_radius = inner_diameter / 2;
	outer_diameter = inner_diameter + (thickness * 2);

	difference()
	{
		cylinder(h = width, d = outer_diameter);
		cylinder(h = width, d = inner_diameter);

		linear_extrude(width) pie_slice(outer_diameter, angle);
	}

	translate([inner_radius,0,0])
		cube([thickness, length, width]);

	rotate([0,0,angle-180])
		translate([-(inner_radius + thickness),0,0])
			cube([thickness, length, width]);
}

module keyhole(d1, d2, height, width)
{
	cylinder(d=d1, h=width);
	translate([height, 0, 0])
		cylinder(d=d2, h=width);
	translate([0,-(d2/2),0])
		cube([height,d2,width]);
}