$fn=64;

id = [50, 50, 40];
wall = 2;

bottom();

translate([0,60,0])
{
    top();
}

module bottom()
{
    box();
    
    for (i = [0 : 1 : 3])
    {
        xOff = i == 1 || i == 2 ? id[0] : 0;
        yOff = i == 2 || i == 3 ? id[1] : 0;
        translate([wall + xOff, wall + yOff, wall])
        {
            rotate([0,0,i * 90])
                cornerPost(3.5, 2);
        }
    }
}

module top()
{
    difference()
    {
        union()
        {
            box();
            for (i = [0 : 1 : 3])
            {
                xOff = i == 1 || i == 2 ? id[0] : 0;
                yOff = i == 2 || i == 3 ? id[1] : 0;
                translate([wall + xOff, wall + yOff, wall])
                {
                    rotate([0,0,i * 90])
                    {
                        cornerPost(3.5, 0);
                    }
                }
            }
        }
        
        for (i = [0 : 1 : 3])
        {
            xOff = i == 1 || i == 2 ? id[0] : 0;
            yOff = i == 2 || i == 3 ? id[1] : 0;
            translate([wall + xOff, wall + yOff, wall])
            {
                rotate([0,0,i * 90])
                {
                    translate([3.5, 3.5, -(wall + 0.1)])
                    {
                        cylinder(h=(id[2]/2)+wall+0.2, d=3.5);
                        cylinder(h=(id[2]/2)-2, d=6);
                    }
                }
            }
        }
    }
}

module box()
{
    difference()
    {
        cube([id[0] + (wall * 2), id[1] + (wall * 2), (id[2] / 2) + wall]);
        
        translate([wall,wall,wall])
        {
            cube([id[0], id[1], id[2]]);
        }
        
        yMid = (id[0] + (wall * 2))/2;
        
        translate([-1, yMid + 5, (id[2] / 2) + wall])
            rotate([0,90,0])
                cylinder(d=4, h=5);
        translate([-1, yMid - 5, (id[2] / 2) + wall])
            rotate([0,90,0])
                cylinder(d=4, h=5);
    }
}

module cornerPost(r, r2)
{
    difference()
    {
        union()
        {
            cube([r*2, r, id[2]/2]);
            cube([r, r*2, id[2]/2]);
            translate([r, r, 0])
                cylinder(h=id[2]/2, r=r);
        }
        
        translate([r,r,5])
            cylinder(h=id[2]/2, r=r2);
    }
}