inner_diameter = 20.6;
wall = 3;
length = 20;
fn = 30;
height = 15;
wing = 15;
wing_wall = 3;

outer_diameter = inner_diameter + (wall * 2);
outer_radius = outer_diameter / 2;

bracket();

mirror_copy([0,1,0])
{
    translate([length,outer_radius,-(outer_radius + height)])
    {
        wing();
    }
}

module bracket()
{
    rotate([0,90,0])
    {
        difference()
        {
            union()
            {
                cylinder(d = outer_diameter, h = length, $fn=fn);
                sphere(d = outer_diameter, $fn=fn);
                           
                translate([0, -outer_radius, 0])
                {
                    cube([outer_radius + height, inner_diameter + (wall * 2), length]);
                }
                rotate([0,90,0])
                {
                    rotate([0,0,(360/fn)/2])
                    {
                        cylinder(d = outer_diameter, h = outer_radius + height, $fn=fn);
                    }
                }
            }
            
            translate([0,0,wall])
            {
                cylinder(d = inner_diameter, h = length - wall + 0.1, $fn=fn);
            }
        }
    }
}

module wing()
{
    rotate([0,-90,0])
    {
        difference()
        {
            linear_extrude(length)
            {
                polygon([[0,0],[0,wing],[wall, wing],[height + outer_radius,0],[0,0]]);
            }
            
            translate([wing_wall,0,wing_wall])
            {
                cube([height + outer_radius, wing, length - (wing_wall * 2)]);
            }
            
            rotate([0,-90,0])
            {
                translate([length / 2, (wing / 2), -wing_wall])
                {
                    countersink(4.3, 8, wing_wall, 3);
                }
            }
        }
    }
}

module mirror_copy(v = [1, 0, 0])
{
    children();
    mirror(v) children();
}

module countersink(cd1, cd2, l, cl)
{
    cylinder(h = l, d1 = cd1, d2 = cd1, $fn=fn);
    cylinder(h = cl, d1 = cd2, d2 = cd1, $fn=fn);
}