include <modules.scad>

$fn = 60;

pots(6);

//hook(14.5, 5, 5);


module post(length)
{
    rotate([-90,0,0])
    {
        cylinder(length-2.5, d=4.6);
        translate([0,0,length-2.5])
            sphere(d=4.6);
    }
}

module hook(length, diameter, overhang)
{
 
    rotate([-90,0,0])
    {
        cylinder(7, d = 5.5);
        cylinder(length, d = diameter);
    }
    
    translate([0,length,-overhang])
        cylinder(overhang, d = diameter);
    
    translate([0,length,0])    
        sphere(d=diameter);
    
    translate([0,length,-overhang])
        sphere(d=diameter);
}

module pots(count)
{
    difference()
    {
        union()
        {
            for (i = [0 : 1 : count-1])
            {
                translate([i * 40, 0, 0])
                {
                    cylinder(26, d1=42, d2=42);
                                 
                    translate([0, 21, 2.5])
                    {
                        post(9);
                    }                  
               }          
            }
            
            // Hook
            for (i = [0 : 1 : count-2])
            {
                translate([(i * 40) + 20, 21, 22.5])
                {             
                    //hook();
                }          
            }
            
            // Backing
            translate([0,0,0])
            {
                cube([40 * (count - 1), 42/2, 26]);
            }

        }
        
        // Hole for hooks
        for (i = [0 : 1 : count-2])
        {
            translate([(i * 40) + 20, 24.1, 22.5])
            {
                rotate([90,0,0])
                {
                    cylinder(10, d=5.5);
                }
            }          
        }
               
        // Main pot cutout
        for (i = [0 : 1 : count-1])
        {
            translate([i * 40, 0, 1])
            {                                 
                cylinder(45, d=34);
            }
        }       
    }
}