use <threads.scad>

f=30;
topThreadDepth = 5;
innerTapDiameter = 19;
wall = 5;
cutoutDepth = 27 + 5;
sealDepth = 6.5 + 0.5;
hoseDepth = 14;
outerHoseDiameter = 7.4;
tapWidth = 22;

innerTapRadius = innerTapDiameter / 2;
outerTapRadius = innerTapRadius + wall;
outerHoseRadius = outerHoseDiameter / 2;
innerHoseRadius = outerHoseRadius - 1;

mainCylinderHeight = topThreadDepth +
                     cutoutDepth +
                     sealDepth +
                     wall;

difference() {
    difference() {
        union() {
            // Main cylinder
            cylinder(mainCylinderHeight, outerTapRadius, outerTapRadius, $fn=f);

            // Hose cylinder
            translate([0, 0, mainCylinderHeight]) {
                cylinder(hoseDepth, outerHoseRadius + 1, outerHoseRadius);
            }
        }
        
        // Hose cutouts
        translate([0, 0, mainCylinderHeight]) {
            cylinder(hoseDepth, innerHoseRadius + 1, innerHoseRadius);
        }
        translate([0, 0, topThreadDepth + cutoutDepth + sealDepth]) {
            cylinder(wall, innerHoseRadius + 1, innerHoseRadius + 1);
        }
        
        // Seal cutout
        translate([0, 0, topThreadDepth + cutoutDepth]) {
            cylinder(sealDepth, innerTapRadius, innerTapRadius, $fn=f);
        }
        
        // Tap Cutout
        translate([0, 0,(cutoutDepth / 2) + topThreadDepth]) {
            cube([tapWidth,tapWidth + 10,cutoutDepth], center = true);
        }
        
        // Thread
        metric_thread (16, 1.5, topThreadDepth, internal = true);
    }
}

threadLength = 15;
handleWidth = 3;
    
//translate([30,0,0]) {
//    union() {
//        rotate([90,0,0]) {
//            translate([-4, threadLength + 4.5, -(handleWidth / 2)]) {
//                cylinder(handleWidth, 6, 6);
//            }
//            translate([4, threadLength + 4.5, -(handleWidth / 2)]) {
//                cylinder(handleWidth, 6, 6);
//            }
//            
//        }
//    
//        metric_thread (15, 1.5, threadLength, internal = false);
//    }
//}