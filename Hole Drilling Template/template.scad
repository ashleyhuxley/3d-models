include <modules.scad>

$fn=30;

width = 49;
length = 58;

line_width = 4;


w1 = line_width;
w2 = line_width / 2;

difference()
{
    union()
    {
        quads(width, length)
        {
            cylinder(h=0.6, d=6);
        }
        translate([-(length/2),-(width/2),0])
        {
            cube([length, width, 0.6]);
        }
    }
    
    quads(width, length)
    {
        cylinder(h=0.6, d=2);
    }
    
    translate([-(length/2),-(width/2),0])
    {
        linear_extrude(1)
        {
            polygon([[w2,w1],
                     [(length/2)-w2, width/2],
                     [w2, width-w1]]);
            polygon([[w1,w2],
                     [(length/2), (width/2)-w2],
                     [length-w1, w2]]);
            polygon([[length-w2,w1],
                     [(length/2)+w2, (width/2)],
                     [length-w2, width-w1]]);
            polygon([[w1,width-w2],
                     [(length/2), (width/2)+w2],
                     [length-w1, width-w2]]);
        }
    }
}