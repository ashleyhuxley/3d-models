length = 60;
height = 7;
width = 27;
offset = 15;

difference()
{
	union()
	{
		cube([width, length, height]);
		translate([0, 0, height]) cube([9, length, 2]);
		translate([width - 9, 0, height]) cube([9, length, 2]);
	}

	translate([width / 2, offset, 0])
	{
		translate([0,0,-0.1]) cylinder(h = height + .2, d = 7, $fn=40);
		translate([8,0,height / 2]) cube([width, 10.4, 3.4], center=true);
	}

	translate([width / 2, length - offset, 0])
	{
		translate([0,0,-0.1]) cylinder(h = height + .2, d = 7, $fn=40);
		translate([8,0,height / 2]) cube([width, 10.4, 3.4], center=true);
	}
}