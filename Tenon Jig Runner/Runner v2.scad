length = 120;

difference()
{
	union()
	{
		cube([35, length, 10]);
		translate([12, 0, -5]) cube([6, length, 6]);
		translate([30, 0, -5]) cube([5, length, 6]);
	}

	translate([-1,30,5]) rotate([0,90,0]) cylinder(h = 37, d=4.5, $fn=40);
	translate([-1,length - 30,5]) rotate([0,90,0]) cylinder(h = 37, d=4.5, $fn=40);

	translate([0,30,5]) rotate([0,90,0]) cylinder(h = 8.5, d=5.2, $fn=40);
	translate([0,length - 30,5]) rotate([0,90,0]) cylinder(h = 8.5, d=5.2, $fn=40);
}