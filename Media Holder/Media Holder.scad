difference()
{
    cube([175, 53.4, 10]);
    
    translate([5, 5, 2])
    {
        translate([0,0,0])
            Space(3, 8)
                CF();

        translate([25,0,0])
            Space(12, 8)
                SD();
        
        translate([25,30.9,0])
            Space(12, 8)
                MSD();
        
        translate([138,0,0])
            rotate([0,0,90])
                Space(4, 12.8)
                    USB();
        
        translate([165,0,0])
            rotate([0,0,90])
                Space(4, 12.8)
                    USB();
            
    }
}

module Space(count, spacing)
{
    for (i = [0 : 1 : count - 1])
    {
        translate([i * spacing, 0, 0])
        {
            children();
        }
    }
}

module CF()
{
    cube([3.7, 43.4, 10]);
}

module SD()
{
    cube([2.3, 25.4, 10]);
}

module MSD()
{
    cube([1.1, 12.5, 10]);
}

module XD()
{
    cube([2.1, 25.4, 10]);
}

module USB()
{
    cube([4.9, 12.4, 10]);
}