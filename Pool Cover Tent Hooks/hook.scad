include <modules.scad>

$fn = 60;
n = 0.1;

wall = 3;
innerHookDia = 51;
outerHookDia = innerHookDia + (wall * 2);

innerHookRad = innerHookDia / 2;
outerHookRad = outerHookDia / 2;

hookWidth = 24;
hookLength = 50;
hookAngle = 130;

poleDepth = 30;
poleOffset = 7;
poleOd = 14;
poleId = 11;

hook();

module hook()
{
    difference()
    {
        cylinder(hookWidth, d = outerHookDia);
        translate([0,0,-n])
        {
            cylinder(hookWidth + n*2, d = innerHookDia);
            linear_extrude(hookWidth + n*2)
                pie_slice(outerHookRad + n*2, hookAngle);
        }
    }

    translate([innerHookRad + 2,0,0])
    {
        cylinder(hookWidth, d=4);
    }

    rotate([0,0,-(180 - hookAngle)])
    {
        translate([-innerHookRad - wall, 0, 0])
        {
            payload();
        }
    }
}

module payload()
{
    // Rounded End
    rotate([0,90,0])
        translate([-(hookWidth/2),hookLength,0])
            cylinder(wall, d=hookWidth);
    
    // Main hook body
    cube([wall,hookLength,hookWidth]);
    
    // Pole holder
    translate([0,hookLength,hookWidth/2])
    {
        difference()
        {
            union()
            {
                translate([-poleOffset,-poleDepth,-(poleOd/2)])
                {
                    cube([poleOffset,poleDepth,poleOd]);
                }
                
                translate([-poleOffset,0,0])
                {
                    rotate([90,0,0])
                    {
                        cylinder(poleDepth, d=poleOd);
                    }
                }
            }
            
            translate([-poleOffset,0,0])
            {
                rotate([90,0,0])
                {
                    translate([0,0,2])
                        cylinder(poleDepth, d=poleId);
                }
            }          
            
        }
    }
}
