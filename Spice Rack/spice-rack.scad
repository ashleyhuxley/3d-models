include <modules.scad>;

$fn = 30;

bracket_width = 12;
inset = 15;
bar_dia = 4.5; // Mid = 4.5, End = 4.3

bracket();

module bracket()
{
	difference()
	{
		rotate([90,0,0])
		{
			linear_extrude(bracket_width)
			{
				polygon(points=[
					[0,0],
					[65,0],
					[65,35],
					[20,80],
					[0,80],
					[20,35],
					[55,35],
					[20,70]
				],
				paths=[
					[0,1,2,3,4],
					[5, 6, 7]
				], convexity=10);
			}
		}

		translate([0,-inset,5])
		{
			color([0,0,1]) cube([61, inset + 0.1, 3.65]);
		}

		translate([55,0.1,30])
		{
			color([0,0,1]) rotate([90,0,0]) cylinder(h=inset, d=bar_dia);
		}

		translate([2,-11,45])
		{
			cube([5, 10, 25]);
		}

		translate([-0.1,-6,53])
			rotate([180,0,0])
				rotate([0,90,0])
					keyhole(10, 4.5, 10, 3);

		translate([10,-(bracket_width/2),0])
			countersunk_hole(3, 6, 2, 6);
	}
}






//translate([0,0,5])
//	cube([60, 500, 3.6]);

//translate([55,0,30])
//	rotate([-90,0,0])
//		cylinder(h=500,d=4);

//translate([25, 30, 11])
//	spice();

module spice()
{
	cylinder(d=45, h=84, $fn=50);
}