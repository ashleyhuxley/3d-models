difference()
{
	cylinder(h = 22, d = 52, $fn=100);
	translate([0,0,1])
	{
		cylinder(h = 25, d = 47.5, $fn=100);
	}

	cylinder(h = 2, d = 30, $fn=100);

	translate([-26, 0, 0])
		cube([52, 26, 22]);
}