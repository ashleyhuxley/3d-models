$fn = 60;

od1 = 60;
od2 = 26;

difference()
{
    union()
    {
        cylinder(d=od1, h=2);
        translate([0,0,2])
        {
            cylinder(d1=od1, d2=od2, h=20);
        }
    }
    
    translate([0,0,-0.1])
    {
        cylinder(d=20.5, h=22.2);
    
        for (i = [0: 1: 2])
        {
            rotate([0, 0, i * (360/3)])
            {
                translate([20, 0, 0])
                {
                    cylinder(d=3.8, h=30);
                    translate([0,0,2])
                    {
                        cylinder(d=8.5, h=30);
                    }
                }
            }
        }
    }
}