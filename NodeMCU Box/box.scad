include <modules.scad>
$fn = 30;

//translate([35.4, 3.3, 7])
	//rotate([0,180,0])
		//mcu();

//translate([2.3,2.3,35]) color([0,0,1]) lid();

boxy();

module boxy()
{
	difference()
	{
		box(40, 75, 30, 2);
		translate([20,1,8])
			cube([14, 2.2, 6], center=true);

		translate([20,1,28.4])
			cube([8, 2.2, 1.2], center=true);

		translate([20,75.5,12])
			rotate([90,0,0])
				cylinder(d=5.5, h=3);

		translate([14,72.3,28.8])
			rotate([0,90,0])
				cylinder(d = 2.5, h = 12);
	}


	translate([7.5,7.5,2])
	{
		translate([0,0,0]) rotate([0,0,0]) standoff(8, 4, 4, 8, 1.5, 1.5);
		translate([0,52,0]) rotate([0,0,-90]) standoff(8, 4, 4, 8, 1.5, 1.5);
		translate([25,0,0]) rotate([0,0,90]) standoff(8, 4, 4, 8, 1.5, 1.5);
		translate([25,52,0]) rotate([0,0,180]) standoff(8, 4, 4, 8, 1.5, 1.5);
	}

	translate([2,2,2])
	{
		translate([0,0,0]) cube([2,2,26]);
		translate([0,69,0]) cube([2,2,26]);
		translate([34,0,0]) cube([2,2,26]);
		translate([34,69,0]) cube([2,2,26]);
	}

	translate([2,65,2])
		cube([38,8,4]);
}


module lid()
{
	width = 35.4;
	clipWidth = 8;

	difference()
	{
		union()
		{
			cube([width,70.4,2]);
			translate([(width/2)-4, -1, 0])
				cube([8, 1, 0.8]);
		}

		translate([(width/2)-((clipWidth+2)/2), 60.4, -0.1])
			cube([clipWidth + 2,10.1,2.2]);
	}

	translate([(width/2)-(clipWidth / 2), 70.8, 0.90])
	{
		rotate([0,90,0])
			cylinder(h=clipWidth, d=2);
		translate([0,-4,-1])
		{
			difference()
			{
				cube([clipWidth, 4, 2]);
				translate([1, 1, 1.4])
					rotate([-15,0,0])
						cube([clipWidth-2, 1.5, 1.1]);
			}
		}
	}

	translate([(width/2)-(clipWidth / 2),67,-4])
	{
		rotate([90,5,90])
		{
			clip(clipWidth, 210, 4, 1.5, 6);
		}
	}

	translate([(width/2)-(clipWidth / 2),59.85,-1.5])
	{
		rotate([90,-90,90])
		{
			clip(clipWidth, 290, 4, 1.5, 0);
		}
	}
}

module mcu()
{
	color([1,0,0])
	{
		difference()
		{
			cube([30.8, 57.5, 1.6]);
			translate([15.4,28.75,-0.1])
			{
				quads(52,25)
				{
					cylinder(h = 1.8, d = 4);
				}
			}
		}

		translate([11.5, -1, 1.6])
			cube([7.8,6,2.3]);

		translate([9.4, 34.3, 1.6])
			cube([12,15,2.3]);

		translate([0, 10, -2.5])
		{
			cube([2.5,38,2.5]);
			for (i = [0 : 14])
			{
				translate([1,1+(i*2.54),-5.75])
					cube([0.5,0.5, 5.75]);
			}
		}
		translate([28.3, 10, -2.5])
		{
			cube([2.5,38,2.5]);
			for (i = [0 : 14])
			{
				translate([1,1+(i*2.54),-5.75])
					cube([0.5,0.5, 5.75]);
			}
		}
	}
}

module standoff(dia, height, id, ih, addx, addy)
{
	rad = dia / 2;

	difference()
	{
		union()
		{
			cylinder(d = dia, h = height);
			translate([-(rad+addx), -rad-addy, 0])
				cube([rad+addx, dia + addy, height]);
			translate([-rad, -(rad+addy), 0])
				cube([dia, rad+addy, height]);
		}

		translate([0,0,height-ih])
			cylinder(d = id, h = ih + 0.1);
	}
}