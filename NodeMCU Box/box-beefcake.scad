include <modules.scad>
include <boards.scad>

$fn = 30;

translate([35.4, 3.3, 10])
	rotate([0,180,0])
		mcu_azdelivery();

translate([40, 3.3, 10])
	beefcake_relay();

translate([30,68,10])
	chocblock_20A(3);

//translate([2.3,2.3,35]) color([0,0,1]) lid();



boxy(75, 105, 35, 2);

module boxy(length, width, height, wall)
{
	inner_length = length - (wall * 2);
	inner_width = width - (wall * 2);

	difference()
	{
		box(length, width, height, wall);

		// Power
		translate([20, wall / 2, 8])
			cube([14, wall + .2, 6], center=true);

		// Lid catch (bottom)
		translate([length / 2, wall / 2, height - 1.6])
			cube([8, wall + .2, 1.2], center=true);

		// Lid catch (top)
		translate([(length / 2)-6, width-2.7, height - 1.2])
			rotate([0,90,0])
				cylinder(d = 2.5, h = 12);
	}

	// Lid stoppers
	translate([wall,wall,height-3-2])
	{
		corners(inner_length, inner_width)
		{
			internal_corner(3);
		}
	}

}


module lid()
{
	width = 35.4;
	clipWidth = 8;

	difference()
	{
		union()
		{
			cube([width,70.4,2]);
			translate([(width/2)-4, -1, 0])
				cube([8, 1, 0.8]);
		}

		translate([(width/2)-((clipWidth+2)/2), 60.4, -0.1])
			cube([clipWidth + 2,10.1,2.2]);
	}

	translate([(width/2)-(clipWidth / 2), 70.8, 0.90])
	{
		rotate([0,90,0])
			cylinder(h=clipWidth, d=2);
		translate([0,-4,-1])
		{
			difference()
			{
				cube([clipWidth, 4, 2]);
				translate([1, 1, 1.4])
					rotate([-15,0,0])
						cube([clipWidth-2, 1.5, 1.1]);
			}
		}
	}

	translate([(width/2)-(clipWidth / 2),67,-4])
	{
		rotate([90,5,90])
		{
			clip(clipWidth, 210, 4, 1.5, 6);
		}
	}

	translate([(width/2)-(clipWidth / 2),59.85,-1.5])
	{
		rotate([90,-90,90])
		{
			clip(clipWidth, 290, 4, 1.5, 0);
		}
	}
}







module standoff(dia, height, id, ih, addx, addy)
{
	rad = dia / 2;

	difference()
	{
		union()
		{
			cylinder(d = dia, h = height);
			translate([-(rad+addx), -rad-addy, 0])
				cube([rad+addx, dia + addy, height]);
			translate([-rad, -(rad+addy), 0])
				cube([dia, rad+addy, height]);
		}

		translate([0,0,height-ih])
			cylinder(d = id, h = ih + 0.1);
	}
}