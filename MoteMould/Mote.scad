include <modules.scad>

difference()
{
	union()
	{
		translate([-60,-30,0])
		{
			//cube([120, 180, 1.6]);
			linear_extrude(1.6)
			{
				polygon(points=[
					[0,30],
					[55,-2],
					[65,-2],
					[120,30],
					[120,150],
					[65,182],
					[55,182],
					[0,150]
				]);
			}
		}

		mirror_copy([1,0,0])
		{
			translate([18,0,0])
			{
				mote();
				translate([0,60,0]) mote();
				translate([0,120,0]) mote();
				rotate([0,0,180]) translate([-20,-30,0]) mote();
				rotate([0,0,180]) translate([-20,-90,0]) mote();
			}
		}
	}

	mirror_copy([1,0,0])
	{
		translate([18,0,0])
		{
			mote_neg();
			translate([0,60,0]) mote_neg();
			translate([0,120,0]) mote_neg();
			rotate([0,0,180]) translate([-20,-30,0]) mote_neg();
			rotate([0,0,180]) translate([-20,-90,0]) mote_neg();
		}
	}
}

module mote()
{
	cylinder(h=50, d1=70, d2=0, $fn=3);
}

module mote_neg()
{
	cylinder(h=45, d1=65, d2=0, $fn=3);
}