include <roundedrect.scad>

id = 41.4;
thickness = 1.5;
id2 = 25;

fn = 60;

translate([0,0,17])
{
    inlet();
    cylinder(h = 2, d = id + (thickness * 2), $fn=fn);
}

module inlet()
{
    difference()
    {
        union()
        {
            cylinder(h = 50, d = id + (thickness * 2), $fn=fn);
            translate([15,0,25])
                rotate([0,90,0]) cylinder(h = 30, d = id2 + (thickness * 4), $fn=fn);
        }
        
        cylinder(h = 50, d = id, $fn=fn);
            translate([15,0,25])
                rotate([0,90,0]) cylinder(h = 30, d1 = id2, d2 = id2 + 0.5, $fn=fn);
    }
}


translate([-22.5, -22.5, 14])
    rounded_rect(45, 45, 5, 3);

difference()
{
    translate([-18, -17.5, 1.5])
        cube([36, 35, 14]);
    
    translate([-19, -16, 1.5])
        cube([38, 32, 14]);
}