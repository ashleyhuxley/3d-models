fn = 60;

difference()
{
    cylinder(h = 1, d = 50, $fn=fn);
    translate([0,0,-0.1]) cylinder(h = 1.7, d = 6.4, $fn=fn);
}