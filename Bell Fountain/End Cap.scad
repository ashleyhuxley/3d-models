fn = 60;

id = 41.5;
thickness = 1.5;
height = 10;

difference()
{
    cylinder(h = height + thickness, d = id + (thickness * 2), $fn=fn);
    translate([0,0,thickness])
    {
        cylinder(h = height, d = id, $fn=fn);
    }
}