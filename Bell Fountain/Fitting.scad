fn = 60;

id = 41.5;
thickness = 1.5;
inner_height = 15;
offset = 15;

full_height = inner_height + offset;



outer();

translate([0,0,offset]) 
{
    inner();

    for (i = [0 : 1 : 2])
    {
        rotate([0,0,i * 120]) translate([5.5, -(thickness/2), 0]) cube([16,thickness,inner_height - 5]);
    }
}

module outer()
{
    difference()
    {
        cylinder(h = full_height, d = id + (thickness * 2), $fn=fn);
        translate([0,0,-0.1]) cylinder(h = full_height + .2, d = id, $fn=fn);
    }
}

module inner()
{
    difference()
    {
        cylinder(h = inner_height, d = 14, $fn=fn);
        translate([0,0,-0.1]) cylinder(h = inner_height + .2, d1 = 8.3, d2=9.2, $fn=fn);
    }
}