fn = 40;

h1 = 32;
h2 = 15;
h3 = 25;

wall = 3;

d1 = 48;
d2 = 32;

r1 = d1 / 2;
r2 = d2 / 2;

r1in = r1 - wall;
r2in = 25 / 2;

difference() {
    cylinder(h1, r1, r1 + 0.4, $fn = fn);
    cylinder(h1, r1in, r1in, $fn = fn);
}

translate([0,0,h1]) {
    difference() {
        cylinder(h2, r1 + 0.4, r2, $fn = fn);
        cylinder(h2, r1in, r2in, $fn = fn);
    }
}

translate([0,0,h1 + h2]) {
    difference() {
        cylinder(h3, r2, r2, $fn = fn);
        cylinder(h3, r2in, r2in + .5, $fn = fn);
    }
}