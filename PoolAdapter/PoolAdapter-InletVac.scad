fn = 40;

h1 = 10;
h2 = 2;
h3 = 15;

wall = 3;

d1 = 28;
d2 = 32;


difference() {
    cylinder(h1, d = d1 + wall, $fn = fn);
    cylinder(h1, d = d1, $fn = fn);
}

translate([0,0,h1]) {
    difference() {
        cylinder(h2, d1 = d1 + wall, d2 = d2, $fn = fn);
        cylinder(h2, d1 = d1, d2 = d2 - wall, $fn = fn);
    }
}

translate([0,0,h1 + h2]) {
    difference() {
        cylinder(h3, d = d2, $fn = fn);
        cylinder(h3, d = d2 - wall, $fn = fn);
    }
}