fn = 40;

h1 = 30;
h2 = 15;
h3 = 25;

wall = 3.5;

d1 = 42;
d2 = 32;

r1 = d1 / 2;
r2 = d2 / 2;

r1in = r1 - wall;
r2in = (25 / 2) + 0.3;

difference() {
    union()
    {
        cylinder(h1, r1, r1 + 0.4, $fn = fn);
        cylinder(3, r1, r1 + 2, $fn=fn);
        translate([0,0,3]) cylinder(0.7,r1 + 2, r1 + 2, $fn=fn);
        translate([0,0,3.7]) cylinder(0.5, r1 + 2, r1, $fn=fn);
    }
    cylinder(h1, r1in, r1in, $fn = fn);
}

translate([0,0,h1]) {
    difference() {
        cylinder(h2, r1 + 0.4, r2, $fn = fn);
        cylinder(h2, r1in, r2in, $fn = fn);
    }
}

translate([0,0,h1 + h2]) {
    difference() {
        cylinder(h3, r2, r2, $fn = fn);
        cylinder(h3, r2in, r2in + .5, $fn = fn);
    }
}