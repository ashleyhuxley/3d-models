width = 13;
thickness = 2;
fn = 30;
tabs = 13;
inner_diameter = 15;
hole = 3.8;

inner_radius = inner_diameter / 2;
outer_diameter = inner_diameter + (thickness * 2);
outer_radius = outer_diameter / 2;
length = (tabs * 2) + outer_diameter;

difference()
{
    union()
    {
        cube([length, width, thickness]);

        translate([tabs, 0, 0])
        {
            cube([outer_diameter, width, outer_radius]);
        }

        translate([length / 2, 0, outer_radius])
        {
            rotate([-90,0,0])
            {
                cylinder(h = width, r=outer_radius, $fn=fn);
            }
        }
    }
    
    translate([tabs + thickness, 0, 0])
    {
        cube([inner_diameter, width, inner_radius + thickness]);
    }

    translate([length / 2, 0, outer_radius])
    {
        rotate([-90,0,0])
        {
            cylinder(h = width, r=inner_radius, $fn=fn);
        }
    }
    
    translate([tabs / 2, width / 2, 0])
        cylinder(h = thickness, d = hole, $fn=fn);

    translate([length - (tabs / 2), width / 2, 0])
        cylinder(h = thickness, d = hole, $fn=fn);
}