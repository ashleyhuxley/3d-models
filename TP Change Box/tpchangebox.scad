include <modules.scad>
$fn = 30;

translate([2.3,2.3,26.5]) color([0,0,1]) lid();

//translate([2, 4, 2]) battery_pack();   
//translate([22,24,15]) rotate([90,0,0]) button();

boxy();

module boxy()
{
	difference()
	{
		box(34, 62, 28.5, 2);

		translate([17,1,26.9])
			cube([8, 2.2, 1.2], center=true);

		translate([22,2.8,15])
			rotate([90,0,0])
				cylinder(d=13, h=3);

		translate([11,59.3,27.3])
			rotate([0,90,0])
				cylinder(d = 2.5, h = 12);
	}

	translate([2,2,2])
	{
		translate([0,0,0]) cube([2,2,24.5]);
		translate([0,56,0]) cube([2,2,24.5]);
		translate([28,0,0]) cube([2,2,24.5]);
		translate([28,56,0]) cube([2,2,24.5]);
	}

}


module lid()
{
	width = 29.4;
    length = 57.4;   // 70.4
    
	clipWidth = 8;

	difference()
	{
		union()
		{
			cube([width,length,2]);
			translate([(width/2)-4, -1, 0])
				cube([8, 1, 0.8]);
		}

		translate([(width/2)-((clipWidth+2)/2), length-10, -0.1])
			cube([clipWidth + 2,10.1,2.2]);
	}

	translate([(width/2)-(clipWidth / 2), length + 0.4, 0.90])
	{
		rotate([0,90,0])
			cylinder(h=clipWidth, d=2);
		translate([0,-4,-1])
		{
			difference()
			{
				cube([clipWidth, 4, 2]);
				translate([1, 1, 1.4])
					rotate([-15,0,0])
						cube([clipWidth-2, 1.5, 1.1]);
			}
		}
	}

	translate([(width/2)-(clipWidth / 2),length-3.4,-4])
	{
		rotate([90,5,90])
		{
			clip(clipWidth, 210, 4, 1.5, 6);
		}
	}

	translate([(width/2)-(clipWidth / 2),length - 10.55,-1.5])
	{
		rotate([90,-90,90])
		{
			clip(clipWidth, 290, 4, 1.5, 0);
		}
	}
}


module battery_pack()
{
    color([0,1,0])
        cube([12,52,24]);
}

module button()
{
    color([0,1,0.5])
        cylinder(d=13, h=22);
}
