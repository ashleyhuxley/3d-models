$fn=60;

id = 21.5 + 0.8;
wall = 1.5;
l1 = 56;
l2 = 50;

od = id + (wall * 2);

difference()
{
    bar();
    
    // Main Cutouts
    cylinder(h=l1, d=id);
    
    translate([0,l2/2+100,l1/2])
        rotate([90,0,0])
            cylinder(h=l2+250, d=id);
}

module bar()
{
    foo();
    
    // Fillet
    difference()
    {
        translate([(od/2),-7,(l1/2)-(od/2)])
            cube([5,5,od]);

        translate([(od/2)+5,-2,(l1/2)-(od/2)])
            cylinder(d=10,h=od);
    }

    // Mounting block
    difference()
    {
        translate([5,-7,l1/2])
            rotate([0,90,30])
                column(14,25,34);
        
        translate([5,-10,l1/2])
            rotate([0,90,30])
                cylinder(d=5, h=50);
    }
    
    // Mounting cylinder
    //translate([10,-10,l1/2])
        //rotate([0,90,30])
            //cylinder(d=14.5, h=34);
}

module foo()
{
    difference()
    {
        union()
        {
            // Cross cylinder
            cylinder(h=l1, d=od);
            
            // Main cylinder
            translate([0,(l2/2)+5,l1/2])
                rotate([90,0,0])
                    cylinder(h=l2, d=od);
            
            // Block
            translate([0,-8,(l1/2)-(od/2)])
                cube([(od/2), 38, od]);
            
            // Finger rest
            translate([0,-7.35,l1/2])
                rotate([0,90,0])
                    cylinder(d=od, h=33+(od/2));       
        }

        // Cutout cube
        translate([(od/2),-7,(l1/2)-(od/2)])
            cube([33,25,od]);
    }
}

module column(w, l, h)
{
    translate([-w/2,-l/2,0])
        cube([w,l,h]);
}