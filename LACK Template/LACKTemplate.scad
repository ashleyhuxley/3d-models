thickness = 0.6;
spacing = 225;
diameter = 5;

difference()
{
    cube([50, 300, thickness]);
    translate([18, (300 - spacing) / 2, -0.1]) cylinder(h = thickness + 0.2, d = diameter);
    translate([18, ((300 - spacing) / 2) + spacing, -0.1]) cylinder(h = thickness + 0.2, d = diameter);
    
    translate([8, ((300 - spacing) / 2) + 20, -0.1]) 
    cube([34, 300 - (((300 - spacing) / 2) + 20) * 2, thickness +0.2]);
}

