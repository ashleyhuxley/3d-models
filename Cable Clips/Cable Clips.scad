l = 15;

difference()
{
    union()
    {
        cube([1, l, 15]);

        rotate([90,0,0])
        {
            translate([1, 0, -l])
            {
                linear_extrude(l)
                {
                    pie_slice(7.5, 90);
                }
            }
        }
    }
    
    translate([3.5,l,2.5])
    {
        rotate([90,0,0])
        {
            cylinder(l, d1 = 5, d2 = 5, $fn=30);
        }
    }
    
    translate([1, 0, 0])
    {
        cube([5, l, 2.5]);
    }
}


module pie_slice(r = 3.0, a = 30)
{
    $fn = 64;
    intersection()
    {
        circle(r=r);
        square(r);
        rotate(a - 90)
            square(r);
    }
}