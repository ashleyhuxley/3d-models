fn=40;
wall = 2.5;


difference()
{
    union()
    {
        cube([wall, 20, 20]);
        cube([20, 20, wall]);
        translate([20, 10, 0])
        {
            cylinder(d = 20, wall, $fn=fn);
        }
    }

    rotate([0,90,0])
    {
        translate([-10, 10, 0])
        {
            cylinder(d = 5.8, wall, $fn=fn);
        }
    }    
    
    translate([wall, 8.5, 0])
    {
        cube([7.6, 15, wall]);
    }
    translate([wall + 5, 3.75, 0])
    {
        cube([12, 12.5, wall]);
    }
    translate([20, 10, 0])
    {
        cylinder(d = 12.5, wall, $fn=fn);
    }
    translate([wall + 4.75, 8.5, 0])
    {
        cylinder(d = 9.5, wall, $fn=fn);
    }
}