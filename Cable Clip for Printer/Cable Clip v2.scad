fn=40;
wall = 2.5;


difference()
{
    union()
    {
        cube([wall, 20, 20]);
        translate([0,10,5])
            rotate([0,0,90])
                torus2(5, 1.5, 180);
    }

    rotate([0,90,0])
    {
        translate([-12, 10, 0])
        {
            cylinder(d = 5.8, wall, $fn=fn);
        }
    }    
    

}

module torus2(r1, r2, a)
{
    rotate_extrude(angle = a)
        translate([r1,0,0])
            circle(r2, $fn=fn);
}