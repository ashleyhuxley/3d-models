height = 21;
width = 66;
wall = 2;

full_width = width + (wall * 2);
full_height = height + wall;

co_width = 55;
co_height = 13;

difference()
{
    cube([full_width, 10, full_height]);
    translate([wall, wall, 0])
    {
        cube([width, 10.1, height]);
    }
    
    translate([(full_width / 2) - (co_width / 2), 0, (height / 2) - (co_height / 2)])
    {
        cube([co_width, wall, co_height]);
    } 
}

translate([(full_width / 2) - (15 / 2), -15 ,0]) wing(15);


module wing(size)
{
    thickness = 4;
    
    difference()
    {
        cube([size, size, thickness]);
        translate([size/2, size/2, -0.1])
        {
            cylinder(h = 5 + 0.2, d = 4.3, $fn = 30);
            translate([0,0,thickness - 3 + 0.15])
                countersink(3, 4.3, 8);
        }
        
    }

}

module countersink(depth, cd1, cd2)
{
    cylinder(h = depth, d1 = cd1, d2 = cd2, $fn=30);
}