keyDia = 16;
wall = 15;
length = 40;
width = 90;

$fn=20;

lock();
//switch();



module lock()
{
    difference()
    {
        union()
        {
            cylinder(d=keyDia + (wall * 2), h=length);
            linear_extrude(length)
            {
                polygon([
                    [-((keyDia/2)+wall),   0],
                    [-(width/2)        , -20],
                    [-(width/2)        , -23],
                    [width/2           , -23],
                    [width/2           , -20],
                    [((keyDia/2)+wall) ,   0]
                ]);
            }
        }
        
        cylinder(d=keyDia, h=length);
        translate([-6, 0, 0])
        {
            cube([12, 30, length]);
        }
        
        translate([-35,-2,20])
        {
            rotate([90,0,0])
            {
                cylinder(h=15, d=12);
                translate([0,0,15])
                {
                    cylinder(h=20, d=3);
                    cylinder(h=3, d1=8, d2=3);
                }
            }
        }
        translate([35,-2,20])
        {
            rotate([90,0,0])
            {
                cylinder(h=15, d=12);
                translate([0,0,15])
                {
                    cylinder(h=20, d=3);
                    cylinder(h=3, d1=8, d2=3);
                }
            }
        }
    }
}

module switch()
{
    color([1,0,0])
    {
        difference()
        {
            cube([28.5, 11, 16.5]);
            
            translate([3,11.5,3])
            {
                rotate([90,0,0])
                {
                    cylinder(h=12, d=3);
                }
            }
            translate([28.5-3,11.5,16.5-3])
            {
                rotate([90,0,0])
                {
                    cylinder(h=12, d=3);
                }
            }
        }
        
        translate([3, 5, 16.5])
        {
            cube([3, 5, 3]);
        }
    }
}