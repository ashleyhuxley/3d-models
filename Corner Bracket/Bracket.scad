fn = 50;

module side()
{
    cube([20, 20, 3]);
    translate([20,0,0]) cube([20, 20, 3]);
    translate([0,20,0]) cube([20, 20, 3]);
    translate([20,20,0]) cylinder(3, d=40, $fn=fn);
}

side();
rotate([90,0,0]) translate([0,0,-3]) side();
rotate([0,-90,0]) translate([0,0,-3]) side();