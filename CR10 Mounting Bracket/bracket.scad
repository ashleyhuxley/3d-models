$fn=30;

tn = 2;
w = 15;

id = 126;
ih = 120;

od = id + tn + tn;
oh = ih + tn;

difference()
{
    cube([od, oh, w]);
    translate([tn,tn,-0.1])
    {
        cube([id, ih+0.1, w+0.2]);
    }
}

translate([od, oh-4, 0])
{
    tab();
}

translate([-20, oh-4, 0])
{
    tab();
}

module tab()
{
    difference()
    {
        cube([20,4,w]);
        
        translate([10,4+0.1,7.5])
        {
            rotate([90,0,0])
            {
                cylinder(d=4.2, h=4+0.2);
            }
        }        
    }
}